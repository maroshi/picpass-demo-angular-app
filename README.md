# Picpass

Keyboard usage for password entry pose security risks. Unpleasant in small screen devices, or in handheld remote controls.
And there is no viable solution for password authentication in voice operated devices.

We suggest a method mapping text password to a short sequence of never repeated pictures. Implementing 4 layers of separation: Password → Password-segments → Secret questions → Secret keywords/answers → Target pictures suggesting to secret keywords/answers.
Wherein the secret keywords/answers can be computed/calculated at runtime.
Also the target pictures are computed at runtime.

This dynamic runtime passwords association with pictures open new business opportunities, and solve mentioned challenges.

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 10.0.1.

## Installation

1. Install `node.js` into your operating system from https://nodejs.org/en/download/
2. Install Angular cli tools: Run command line: `npm install --global @angular/cli`, read more in https://angular.io/cli#cli-overview-and-command-reference 
3. Install http-server web server: Run command line `npm install --global http-server`, read more in https://www.npmjs.com/package/http-server 
4. Install `git` into your operating system from https://git-scm.com/downloads 
5. Install application repository: In a new empty directory run command line `git clone https://maroshi@bitbucket.org/maroshi/picpass-demo-angular-app.git`
6. Build development repository: Change directory into `picpass-demo-angular-app` run command line `npm install` .

## Default configuration and features

__Edit configuration file:__ `src/assets/config.json`
## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Build

Run `npm run build` to build the project. The build artifacts will be stored in the `dist/` directory. If there are any build errors, try command line `ng build --prod   --optimization`

## Publish

Run `npm run publish` to publish the production project on localhost. Navigate to `http://localhost:8888/`.

## Further help
See Picpass soulution site documentation http://homeil.org/documentation

Or contact us from http://homeil.org/contact-us/

Please report problems/bugs in https://bitbucket.org/maroshi/picpass-demo-angular-app/issues

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).
