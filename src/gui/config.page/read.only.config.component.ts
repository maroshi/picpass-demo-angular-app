/*
Copyright (c) 2021, Picpass solutions
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
* Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
* Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
* Neither the name of the Picpass solutions nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL Picpass solutions BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/
import { Component, Input, OnChanges, SimpleChanges } from "@angular/core";
import { ConfigRepository } from "../../dao/config.repository";

@Component({
    selector: "read-only-config",
    templateUrl: "read.only.config.component.html"
})
export class ReadOnlyConfigComponent implements OnChanges{
    public configObj: object;

    ngOnInit(): void {
    }

    @Input() public selectedCategory: string;
    public selectedConfiguration: Object = null;
    public selectedConfigurationElementsArr: [string, any][] = null;
    public currentElementName: string = null;
    public currentElementValue: string = null;

    constructor(private _configRepository: ConfigRepository) {
    }

    ngOnChanges(changes: SimpleChanges): void {
        this.configObj = this._configRepository.getConfigObject(this.selectedCategory);
        this.selectedConfigurationElementsArr = Object.entries(this.configObj);
    }

    get categories(): string[] {
        return this._configRepository.categories;
    }

    isAnObject(currElement: any): boolean {
        let retVal = false;
        return typeof currElement === "object" && !Array.isArray(currElement);
    }

    isAnArray(currElement: any): boolean {
        let retVal = false;
        return typeof currElement === "object" && Array.isArray(currElement);
    }

    isPrimitive(currElement: any): boolean {
        let retVal = false;
        return typeof currElement !== "object";
    }

    getElementsFromCompositeElement(compositeElement: any): Object {
        return Object.entries(compositeElement);
    }

    renderCurrentElement() {
        for (let currElmName in this.selectedConfiguration) {
            console.dir(currElmName);
            let currElmValue = this.selectedConfiguration[currElmName];
            if (typeof currElmValue === "object" && Array.isArray(currElmValue)) {
                this.addArrayElement(currElmName, currElmValue);
            } else if (typeof currElmValue === "object") {
                this.addObjectElement(currElmName, currElmValue);
            } else {
                this.addPrimitiveValue(currElmName, currElmValue);
            }
        }
    }

    addArrayElement(name: string, array: Array<any>) {
        console.log(`name=${name}\t\tArray with  ${array.length} elements`);
    }

    addObjectElement(name: string, obj: object) {
        console.log(`name=${name}\t\tObject with  ${Object.entries(obj).length} elements`);
    }
    addPrimitiveValue(name: string, value: any) {
        console.log(`name=${name}\t\tvalue=${value}`)
        return null;
    }
}