/*
Copyright (c) 2021, Picpass solutions
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
* Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
* Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
* Neither the name of the Picpass solutions nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL Picpass solutions BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/
import { Component, OnInit, Input } from "@angular/core";
import { LayoutService } from 'src/bom/layout.service';
import { PicpassWidgetProxyService } from 'src/bom/picpass.widget.proxy.service';
import { ConfigRepository } from "../../dao/config.repository";
@Component({
  selector: "feature-chkbx",
  template: `
<div class="form-check">
  <Input class="form-check-input"
         type="checkbox"
         [checked]="ifCheckedFeature(featureName)"
         (change)="toggle($event.target, featureName)"/>
  <label class="form-check-label">{{configRepo.featuresObj[featureName][0]}}</label>
</div>
`
})
export class FeatureCheckBoxComponent {
  @Input() public featureName: string;

  constructor(public configRepo: ConfigRepository){}

  toggle(elm: HTMLInputElement, propName: string){
      const checkBoxIs: boolean = elm.checked;
      this.configRepo.featuresObj[propName][1] = checkBoxIs;
  }

  ifCheckedFeature(featrueName: string): boolean {
    return this.configRepo.featuresObj[featrueName][1];
  }
}

@Component({
    selector: "features-config",
    templateUrl: 'features.config.component.html'
})
export class FeaturesConfigComponent  implements OnInit  {
    public featuresObj: object;
    public log:string;
    public keywordsList: string[] = [];

    constructor(public  configRepo: ConfigRepository,
                private _layoutService: LayoutService,
                private _picpassProxy: PicpassWidgetProxyService,){
    }

    async ngOnInit() {
        this._layoutService.currentSelectionsCount = 0;
        this._picpassProxy.rewindPasswordText(1);
        this.featuresObj = ConfigRepository.deepClone(this.configRepo.featuresObj);
        this.log = JSON.stringify(this.featuresObj);

        const featuresConfigElem: HTMLElement = document.getElementById('pp-feature-config');
        const inputElementsArr: NodeListOf<HTMLInputElement> = featuresConfigElem.querySelectorAll(`input,select`);
        inputElementsArr.forEach(inputElm => {
          inputElm.disabled = !this.configRepo.featureFeatureSettings;
        });
        const lableElementsArr: NodeListOf<HTMLLabelElement> = featuresConfigElem.querySelectorAll(`label`);
        lableElementsArr.forEach(labelElm => {
          if (!this.configRepo.featureFeatureSettings)
            labelElm.classList.add('text-muted');
        });
      }

    onApply() {
        // save the form obj to the config obj
        this._picpassProxy.setInitialPasswordValue();

        this._layoutService.initialLayout();
        this.log = JSON.stringify(this.configRepo.featuresObj);
        this.ngOnInit();
    }
}
