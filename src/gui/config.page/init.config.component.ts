/*
Copyright (c) 2021, Picpass solutions
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
* Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
* Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
* Neither the name of the Picpass solutions nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL Picpass solutions BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/
import { Component, Input, OnInit } from "@angular/core";
import { ConfigRepository } from "../../dao/config.repository";
import { LayoutService} from '../../bom/layout.service'
import { PicpassWidgetProxyService } from 'src/bom/picpass.widget.proxy.service';
import { KeywordsRepository } from 'src/dao/keywords.repository';
import { KeywordToPicCountRecord } from 'src/dao/keywordToPicCount.model';

@Component({
    selector: "init-config",
    templateUrl: "init.config.component.html"
})
export class InitConfigComponent implements OnInit{
    @Input() public selectedCategory: string;
    public initObj: object;
    public log:string;
    public keywordsList: string[] = [];

    constructor(public configRepo: ConfigRepository,
        private _layoutService: LayoutService,
        private _picpassProxy: PicpassWidgetProxyService,
        private _keywordsRepo: KeywordsRepository){}

    async ngOnInit() {
        this._layoutService.currentSelectionsCount = 0;
        this._picpassProxy.rewindPasswordText(1);
        this.initObj = ConfigRepository.deepClone(this.configRepo.initObj);
        this.log = JSON.stringify(this.initObj);

        let kwd2picCountRecArr: KeywordToPicCountRecord[] = await  this._keywordsRepo.keywordsToPicCountsSync();
        kwd2picCountRecArr.forEach(k2pc => {
            if(k2pc.picsCount > 5) {
                this.keywordsList.push(k2pc.name)
            }
        });
        // console.log(`InitConfigComponent.ngOnInit`);
        const initConfigElem: HTMLElement = document.getElementById('pp-init-config');
        const inputElementsArr: NodeListOf<HTMLInputElement> = initConfigElem.querySelectorAll(`input,select`);
        inputElementsArr.forEach(inputElm => {
          inputElm.disabled = !this.configRepo.featureInitiationSettings;
        });
        const lableElementsArr: NodeListOf<HTMLLabelElement> = initConfigElem.querySelectorAll(`label`);
        lableElementsArr.forEach(labelElm => {
          if (!this.configRepo.featureInitiationSettings)
            labelElm.classList.add('text-muted');
        });
    }

    onApply() {
        // convert all numeric values to numbers except Password and Keywords
        for (let propertyName of Object.keys(this.initObj)) {
            if (typeof this.configRepo.initObj[propertyName][1] !== 'number')
                continue;

            this.initObj[propertyName][1] = Number(this.initObj[propertyName][1]);
        }
        // save the form obj to the config obj
        // console.dir(this.initObj);
        this.configRepo.initObj = this.initObj;
        this.log = JSON.stringify(this.configRepo.initObj);
    }
}
