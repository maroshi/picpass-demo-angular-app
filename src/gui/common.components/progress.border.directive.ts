/*
Copyright (c) 2021, Picpass solutions
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
* Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
* Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
* Neither the name of the Picpass solutions nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL Picpass solutions BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/
import { Directive, ElementRef } from '@angular/core'
import { Subscription } from 'rxjs';
import { EventDispatchService } from 'src/bom/events.dispatch.service';
import { LayoutService } from 'src/bom/layout.service';
import { ConfigRepository } from 'src/dao/config.repository';

@Directive({
  selector: '[pp-progress-border]'
})
export class ProgressBorderDirective{
  private static colors: string[] = [
    "darkblue",
    "lightblue",
    "green",
    "yellow"
  ];
  private _hostElement: HTMLElement;
  public static borderWidth: number = 2;
  public onSecretKeywordChangeSubscriber: Subscription;
  private static configRepo: ConfigRepository;

  constructor(public element: ElementRef,
              private _layoutService: LayoutService,
              private _eventsService: EventDispatchService,
              public _configRepo: ConfigRepository) {
    this._hostElement = element.nativeElement;
    this._setBorder();
    this.onSecretKeywordChangeSubscriber = this._eventsService
                                               .onChangedSecretKeyword$
                                               .subscribe(msg => {this._setBorder()});
    ProgressBorderDirective.configRepo = this._configRepo;
    ProgressBorderDirective.EnableProgressBorder();
  }


  public static EnableProgressBorder() {
    ProgressBorderDirective.borderWidth = 2;
    if (!ProgressBorderDirective.configRepo.featureColoredBorderIndicator) {
      ProgressBorderDirective.borderWidth = 0;
    }
  }

  private _setBorder() {
    const currentSelection = this._layoutService.currentSelectionsCount;
    const currentColor = ProgressBorderDirective.colors[currentSelection % ProgressBorderDirective.colors.length]
    // console.log(`ProgressBorderDirective currentColor=${currentColor}`);
    this._hostElement.style.border = `${ProgressBorderDirective.borderWidth}px solid ${currentColor}`;
  }
}
