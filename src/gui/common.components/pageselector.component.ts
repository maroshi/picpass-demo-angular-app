/*
Copyright (c) 2021, Picpass solutions
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
* Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
* Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
* Neither the name of the Picpass solutions nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL Picpass solutions BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/
import { Component } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { PicpassWidgetProxyService } from '../../bom/picpass.widget.proxy.service';
import { EventDispatchService } from '../../bom/events.dispatch.service';
import { ConfigRepository } from 'src/dao/config.repository';

@Component({
    selector: 'page-selector',
    template: `
    <div class="float-right px-0 m-0">
        <button id="pp-configuration-btn"
                class="btn btn-primary btn-sm mr-1"
                data-toggle="tooltip" data-placement="top" title="Configuration"
                (click)="onConfiguration()"
                [disabled]="!configRepo.featureConfigurationSettings">
            <i class="fa fa-cogs"></i>
        </button>
        <button id="pp-usersettings-btn"
                class="btn btn-primary btn-sm mr-1"
                data-toggle="tooltip" data-placement="top" title="Subscriber's settigns"
                (click)="onUserSettings()"
                [disabled]="!configRepo.featureSubscriberSettings">
            <i class="fa fa-pencil-square-o"></i>
        </button>
        <button id="pp-picpass-btn"
                class="btn btn-success btn-sm mr-1"
                data-toggle="tooltip" data-placement="top" title="Picpass widget"
                (click)="onPicpass()">
            <i class="fa fa-check-square-o"></i>
        </button>
        <button id="pp-exit-btn"
                class="btn btn-danger btn-sm m-0 pb-0"
                data-toggle="tooltip" data-placement="top" title="Exit without change"
                (click)="onExit()">
            <i class="fa fa-window-close-o fa-2x"></i>
        </button>
    </div>
`})
export class PageSelectorComponent {
    constructor(private _router: Router,
        private _picpassProxy: PicpassWidgetProxyService,
        private _activeRoute: ActivatedRoute,
        private _eventsService: EventDispatchService,
        public  configRepo: ConfigRepository) {
    }

    ngOnInit() {
        let currentPath: string = this._activeRoute.snapshot.url[0].path;
        // remove the icon directing to current page
        let currentButtonElm: HTMLButtonElement = document.querySelector(`#pp-${currentPath}-btn`);
        currentButtonElm.remove();
    }

    onExit() {
        console.log(`clicked on Exit, cancelling all selections and close!`);
        this._picpassProxy.cancelDialog();
    }

    onPicpass() {
        let currentPath: string = this._activeRoute.snapshot.url[0].path;
        if (currentPath.endsWith(`usersettings`)) {
            this._eventsService.onSubmitSecretKeywords(`onSubmitSecretKeywords at: ${new Date().toISOString()}`);
        } else
            this._router.navigateByUrl('/picpass');
    }

    onUserSettings() {
        // console.log(`clicked on UserSettings!`);
        this._router.navigateByUrl('/usersettings');
    }

    onConfiguration() {
        // console.log(`clicked on Configuration!`);
        this._router.navigateByUrl('/configuration');
    }
}
