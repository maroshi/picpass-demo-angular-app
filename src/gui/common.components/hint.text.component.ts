/*
Copyright (c) 2021, Picpass solutions
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
* Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
* Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
* Neither the name of the Picpass solutions nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL Picpass solutions BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/
import { Component, Input , OnInit} from '@angular/core';
import { SecretKeywordsService } from '../../bom/secret.keywords.service';
import { Subscription } from 'rxjs';
import { EventDispatchService } from '../../bom/events.dispatch.service';
import { SecretQuestionFactory } from '../../bom/secret.question.factory'

@Component({
    selector: 'hint-text',
    template: `
<span class="">
  <span *ngIf="!question" id='pp-hint-text' class='pl-0'>
    {{hint}}
  </span>
  <span *ngIf="question" id='pp-hint-text' class='pl-0'>
    {{keywordPrefix}}
  </span>
  <span *ngIf="question" id='pp-hint-text' 
        class="pr-0 font-weight-bold text-info">
    {{hint}}
  </span>
  <span *ngIf="question" id='pp-hint-text'
        [class.ml-n1]="!isDelimeterSpaceRequired()"
        class='pl-0 ml-n1'>
    {{keywordSuffix}}
  </span>
</span>
`})
export class HintTextComponent implements OnInit{
  @Input() keyword: string;
  @Input() question: boolean;
  @Input() updateHintOnEvent: boolean;
  public hint: string;
  public keywordPrefix: string;
  public keywordSuffix: string;
  public suffixSpaced: boolean = false;
  public onSecretKeywordChangeSubscriber: Subscription;

  constructor(private _keywordService: SecretKeywordsService,
              private _eventsService: EventDispatchService){
    this.onSecretKeywordChangeSubscriber = this._eventsService
                                               .onChangedSecretKeyword$
                                               .subscribe(msg => {this._onSecretKeywordChanged(msg)});
  }

  private _onSecretKeywordChanged(msg: string) {
    if (msg === 'initial undefined' || !this.updateHintOnEvent)
      return;
  
    this.keyword = msg;
    this.ngOnInit();
  }

  async ngOnInit() {
    this.hint = this.keyword;
    // console.log(`1 hint:ngOnInit hint=${this.hint}`)
    // console.log(`2 hint:ngOnInit question=${this.question}`)
    if (this.question) {
      if (!this._keywordService.InitiatedFlag) {
        this._keywordService.init();
        // dont wait and abandon this keywords question resolution
        return;
      }
      if (typeof this.keyword === 'undefined') {
        this.keywordPrefix = '';
        this.keywordSuffix = '';
        this.hint = '';
        return;
      }
      const currentCategoryType: number = this._keywordService.categoryTypeByKeyword(this.keyword);
      const templatePartsArr: string[] = SecretQuestionFactory.getQuestionTempalteForSecretKeywordCategoryType(currentCategoryType);
      this.keywordPrefix = templatePartsArr[0];
      this.keywordSuffix = templatePartsArr[1];
    }
  }

  public isDelimeterSpaceRequired(): boolean {
    const suffixFirstChar: string = this.keywordSuffix?.charAt(0);
    return (suffixFirstChar === ' ');
  }
}