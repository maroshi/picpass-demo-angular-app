/*
Copyright (c) 2021, Picpass solutions
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
* Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
* Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
* Neither the name of the Picpass solutions nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL Picpass solutions BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/
import { Component } from "@angular/core";
import { EventDispatchService } from 'src/bom/events.dispatch.service';
import { SecretKeywordsService } from '../../bom/secret.keywords.service';
import { ConfigRepository } from '../../dao/config.repository';
import { AbstractSubscriberSettings } from './abstract.subscriber.settings.class';

@Component({
  selector: 'premium-section',
  templateUrl: 'premium.settings.component.html'
})
export class PremiumSettingsComponent extends AbstractSubscriberSettings {
  public currentRow: number = -1;
  private _lastRow: number = -2;

  constructor(public configRepo: ConfigRepository,
    public keywordsService: SecretKeywordsService,
    public eventsService: EventDispatchService) {
    super(configRepo, keywordsService, eventsService);
  }

  public onKeywordCategoryChange(secretKeywordIdx: number, targetElement: HTMLElement) {
    const newCategory = targetElement.parentElement.innerText;
    const newKeyword = this.keywordsService.getRandomKeywordFromCategory(newCategory);
    this.configRepo.initKeywords[secretKeywordIdx] = newKeyword;
  }

  public setRowId(idx: number): string {
    return `pp-secret-keyword-row-${idx}`;
  }

  public readRowId(i: number) {
    if (this.currentRow == this._lastRow)
      return;
    this.currentRow = i;
    this._lastRow = this.currentRow;
    const currentKeyword = this.configRepo.initKeywords[i];
    this.eventsService.onChangedSecretKeyword(currentKeyword);
  }
  public resetRowId() {
    this.currentRow = -1;
    this.eventsService.onChangedSecretKeyword(undefined);
  }
}