/*
Copyright (c) 2021, Picpass solutions
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
* Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
* Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
* Neither the name of the Picpass solutions nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL Picpass solutions BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/
import { Component, OnInit} from "@angular/core";
import { QuestionRecord } from 'src/dao/question.model';
import { EventDispatchService } from '../../bom/events.dispatch.service';
import { SecretKeywordsService } from '../../bom/secret.keywords.service';
import { ConfigRepository } from '../../dao/config.repository';
import { QuestionsRepository } from '../../dao/questions.repository';
import { AbstractSubscriberSettings } from './abstract.subscriber.settings.class';


@Component({
  selector: 'exclusive-section',
  templateUrl: 'exclusive.serttings.component.html'
})
export class ExclusiveSettingsComponent extends AbstractSubscriberSettings implements OnInit{
  public keywordsCategoryNames: string[] = [
    'Noun / Object', 
    'Verb / Action', 
    'Description', 
    'Place / Location', 
    'Time / Number', 
    'Characther / Person', 
    'Private / Personal'
  ];

  constructor(public configRepo: ConfigRepository,
              public keywordsService: SecretKeywordsService,
              public eventsService: EventDispatchService,
              private _questionsRep: QuestionsRepository) {
    super(configRepo, keywordsService, eventsService);
  }

  public async ngOnInit() {
    let currQuestionsRecsArr: QuestionRecord[] = [];
    const categoryType: string[] = this.keywordsService.keywordsCategoryType;
    currQuestionsRecsArr = await this._questionsRep.placesQuestionsSync();
    this.keywordsService.addQuestionsCategoryToKeywordsMap(categoryType[3], currQuestionsRecsArr);
    currQuestionsRecsArr = await this._questionsRep.numberQuestionsSync();
    this.keywordsService.addQuestionsCategoryToKeywordsMap(categoryType[4], currQuestionsRecsArr);
    currQuestionsRecsArr = await this._questionsRep.charachterQuestionsSync();
    this.keywordsService.addQuestionsCategoryToKeywordsMap(categoryType[5], currQuestionsRecsArr);
    currQuestionsRecsArr = await this._questionsRep.privateQuestionsSync();
    this.keywordsService.addQuestionsCategoryToKeywordsMap(categoryType[6], currQuestionsRecsArr);
  }

  public onKeywordCategoryChange(secretKeywordIdx: number, targetElement: HTMLInputElement) {
    const newCategoryIdx: number = Number(targetElement.value);
    const newCategory: string = this.keywordsService.keywordsCategoryType[newCategoryIdx];
    this.configRepo.initKeywords[secretKeywordIdx] = this.keywordsService.getRandomKeywordFromCategory(newCategory);
  }
}