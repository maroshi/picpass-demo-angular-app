/*
Copyright (c) 2021, Picpass solutions
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
* Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
* Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
* Neither the name of the Picpass solutions nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL Picpass solutions BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/
import { Component, OnInit } from "@angular/core";
import { Router } from '@angular/router';
import { SecretKeywordsService } from '../../bom/secret.keywords.service';
import { PicpassWidgetProxyService } from '../../bom/picpass.widget.proxy.service';
import { LayoutService } from '../../bom/layout.service';
import { ConfigRepository } from '../../dao/config.repository';
import { EventDispatchService } from 'src/bom/events.dispatch.service';
import { SubstriptionTabService } from '../../bom/subscription.tab.service';
import { Subscription } from 'rxjs';

@Component({
    templateUrl: 'subscriber.serttings.component.html'
})
export class SubsriberSettingsComponent implements OnInit{
  private _submitSecretKeywordsEventSubscription: Subscription;
  private _passedInitFlag: boolean = false;


  constructor(private _router: Router,
    public configRepo: ConfigRepository,
    private _layoutService: LayoutService,
    private _keywordsService: SecretKeywordsService,
    private _picpassProxyService: PicpassWidgetProxyService,
    private _eventsService: EventDispatchService,
    public subscriptionTabService: SubstriptionTabService){
    // save current password and keywords settings
    // rewind picpass widget to 0 selections
    this._layoutService.currentSelectionsCount = 0;
    this._picpassProxyService.rewindPasswordText(1);

    this._submitSecretKeywordsEventSubscription = this._eventsService.onSubmitSecretKeywords$
                                                .subscribe(msg => this._onSubmitSecertKeywordsEvent(msg));

  }

  public get currentSecretKeywords(): string[] {
    const currentKeywords: string[] = [];
    for (let i: number = 0; i < this.configRepo.initMaxSelections; i++) {
      currentKeywords.push(this.configRepo.initKeywords[i]);
    }
    return currentKeywords;
  }
  public async ngOnInit() {
    await this._keywordsService.init();
    console.log(`1. Subscriber component initialized ${new Date().toISOString()}`);
    this._passedInitFlag = true;
  }

  public _onSubmitSecertKeywordsEvent(msg: string) {
    if (!this._passedInitFlag)
      return;
    // console.log(msg);
    // this._eventsService.onSubmitSecretKeywords(this._configRepo.initKeywords[0]);
    this.onOK();
  }

  public onOK() {
    if (this.validateKeywords())
      this._router.navigateByUrl('/picpass');
  }

  public validateKeywords(): boolean {
    let validResult: boolean = true;
    const foundQuestion: number = this.currentSecretKeywords.findIndex(kwd => {
      const categoryType: number = this._keywordsService.categoryTypeByKeyword(kwd);
      return categoryType > 2;
    });

    if (foundQuestion > -1) {
      validResult = false;
      const errorMessage: string = `Picpass Demo application has no matching pictures for secret question:

   '${this.currentSecretKeywords[foundQuestion]}'.

Yet... Picpass Demo application is a work in progress.

Change the secret question to secret keyword and apply again.`;
      document.defaultView.alert(errorMessage);
    }

    return validResult;
  }

  public onCancel() {
    // this._keywordsService.logCurrentInitConifg('onCancel() ');
    this.configRepo.initPassword = this._keywordsService.currentInitConfig['Password'];
    // bug fix: deep copy of keywords, prevent shallow copies updates/race-conditions
    this.configRepo.initKeywords = [];
    const keywordsArr: string[] = this._keywordsService.currentInitConfig['Keywords:'];
    keywordsArr.forEach(kwd => this.configRepo.initKeywords.push(kwd));

    this._eventsService.onUpdateKeywordsCategories(`onUpdateKeywordsCategories at: ${new Date().toISOString()}`);
  }

  public onDefault() {
    this.configRepo.resetPassword();
    this.configRepo.resetKeywords();
    this._eventsService.onUpdateKeywordsCategories(`onUpdateKeywordsCategories at: ${new Date().toISOString()}`);
  }
}

