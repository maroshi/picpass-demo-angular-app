/*
Copyright (c) 2021, Picpass solutions
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
* Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
* Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
* Neither the name of the Picpass solutions nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL Picpass solutions BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/
import { Component }    from '@angular/core';
import { Subscription } from 'rxjs';
import { ConfigRepository } from 'src/dao/config.repository';
import { EventDispatchService } from '../../bom/events.dispatch.service';
import { LayoutService, Layout } from '../../bom/layout.service';
import { PicpassWidgetProxyService } from '../../bom/picpass.widget.proxy.service';


@Component({
selector: 'toolbar-section',
template: `
<h5 class="m-0">
<div class="d-flex justify-content-around m-0 text-primary">
    <div class="" (click)="onToggleLayout()"
         [class.pp-removed]="!configRepo.featureLayoutToggleButton">

        <div *ngIf="layoutService.isTextLayout()">
            <i class="fa fa-image fa-2x"></i>
        </div>

        <div *ngIf="layoutService.isImageLayout()">
            <i class="fa fa-file-text fa-2x"></i>
        </div>

    </div>

    <div class=""
         (click)="onRefresh()"
         [class.pp-removed]="!configRepo.featureRefreshLayoutButton">
         <i class="fa fa-repeat fa-2x"></i></div>

    <div class=""
         (click)="onQuit()"
         [class.pp-removed]="!configRepo.featureCompletionButton">
         <i class="fa fa-thumbs-up fa-2x"></i></div>
</div>
</h5>
`})
export class ToolbarComponent{
    private _submitColosePicpassWidget: Subscription;

    constructor(private picpassWidget: PicpassWidgetProxyService,
                public layoutService: LayoutService,
                private eventsService: EventDispatchService,
                public configRepo: ConfigRepository) {
        this._submitColosePicpassWidget = this.eventsService
                                              .onClosePicpassWidget$
                                              .subscribe(msg =>{if(msg != "initial undefined") this.onQuit();});
}

    public onQuit() {
        if (this.configRepo.featurePasswordSubmit){
            this.picpassWidget.submitDialog();
        }
        this.picpassWidget.closeDialog();
    }

    public onRefresh() {
        // console.log(`onRefresh at: ${new Date().toISOString()}`);
        this.eventsService.onRefreshLayout(`onRefresh at: ${new Date().toISOString()}`);
    }
    public onToggleLayout() {
        // console.log(`onRefresh at: ${new Date().toISOString()}`);
        this.eventsService.onToggleLayout(`onToggleLayout at: ${new Date().toISOString()}`);

        if (this.layoutService.isTextLayout()) {
            this.layoutService.layout = Layout.IMAGE;
        } else {
            this.layoutService.layout = Layout.TEXT;
        }
    }
}
