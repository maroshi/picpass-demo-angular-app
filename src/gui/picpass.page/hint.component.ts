/*
Copyright (c) 2021, Picpass solutions
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
* Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
* Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
* Neither the name of the Picpass solutions nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL Picpass solutions BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/
import { Component, OnInit }    from '@angular/core';
import { Subscription } from 'rxjs';
import { ConfigRepository } from 'src/dao/config.repository';
import { EventDispatchService } from '../../bom/events.dispatch.service';
import { LayoutService } from '../../bom/layout.service';

@Component({
selector: 'hint-section',
template: `
<h5  class="m-0 border border-secondary">
<div class="d-flex  justify-content-between">
    <h6 class="m-1 flex-fill">
        <small class="text-muted">{{hintLabel}}:</small>
        <hint-text [keyword]="hintText" 
                   [question]="layoutService.isImageLayout()" updateHintOnEvent="true"
                   [class.pp-removed]="!configRepo.featureHintText"></hint-text>
    </h6>
    <div class="m-0 pr-2 text-secondary pp-show-hint"
        (mouseover)="onHover()"
        (mouseleave)="onLeave()"
        [class.pp-removed]="!configRepo.featureHintHover">
        <i class="fa fa-search"></i>
    </div>
</div>
</h5>
`})
export class HintComponent implements OnInit{
    // public hint: string = 'undefined hint';
    public hintLabel: string = 'Hint';
    public hintText: string = 'h6';
    public onSecretKeywordChangeSubscriber: Subscription;

    constructor(public layoutService: LayoutService,
                public eventsService: EventDispatchService,
                public configRepo: ConfigRepository){
    }

    ngOnInit(): void {
        this.hintText = this.layoutService.targetKeyword;
    }

    onHover() {
        this.eventsService.onHoverHint(`onHoverHint at: ${new Date().toISOString()}`);
    }

    onLeave() {
        this.eventsService.onLeaveHint(`onLeaveHint at: ${new Date().toISOString()}`);
    }
}