/*
Copyright (c) 2021, Picpass solutions
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
* Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
* Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
* Neither the name of the Picpass solutions nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL Picpass solutions BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/
import { Component }    from '@angular/core';
import { bufferTime } from 'rxjs/operators';
import { PicpassWidgetProxyService } from '../../bom/picpass.widget.proxy.service';
import { EventDispatchService } from '../../bom/events.dispatch.service';
import { LayoutService } from '../../bom/layout.service';
import { ConfigRepository } from '../../dao/config.repository';


@Component({
    selector: 'progressbar-section',
    template: `
<h6 class="mb-1 d-flex flex-row text-primary border border-secondary">
    <div id="pp-progresbar-left-arrow" 
         class="pp-progresbar-arrow"
         [class.pp-muted-text]="isInvisibleLeftArrow"
         (click)="onLeftArrowClick">
        
        <i class="fa fa-angle-double-left"></i>
    </div>
    <div id="pp-progressbar-containter" class="d-flex" style="width:100%">
        <div *ngFor="let currPosition of progressBarPositionsArr"
            class="pp-progressbar-position flex-fill text-center"
            [class.pp-muted-text]="isInvisibleLabel(currPosition.label - 1)"
            (click)="onPositionClicked(currPosition.label)">

                {{currPosition.label}}<sup>{{currPosition.superscript}}</sup>
        </div>
    </div>
    <div id="pp-progresbar-right-arrow"
         class="pp-progresbar-arrow"
         [class.pp-muted-text]="isInvisibleRightArrow"
         (click)="onRightArrowClick">

        <i class="fa fa-angle-double-right"></i>
    </div>
</h6>
`})
export class ProgressbarComponent{
    private _possitionOffset: number = 0;
    private _firstPositionFlag: boolean = true;

    constructor(private _configRepo: ConfigRepository,
                private _layoutService: LayoutService,
                private _eventsService: EventDispatchService,
                private _picpassProxyService: PicpassWidgetProxyService) {
    }

    onPositionClicked(positionIdxStr: number): void {
        // console.log(`into onPositionClicked  positionIdxStr=${positionIdxStr}`);
        this._layoutService.currentSelectionsCount = Number(positionIdxStr) - 1;
        this._eventsService.onRefreshLayout(``);
        this._picpassProxyService.rewindPasswordText(Number(positionIdxStr));
    }
    get maxpositionsCount(): number {return Math.max(this._configRepo.featureProgressbarPostions, this._layoutService.currentSelectionsCount);}

    get onRightArrowClick(): boolean {
        if (this.isInvisibleRightArrow)
            return false

        // console.log(`clicked on onRightArrowClick`);
        // console.log(`2. his._firstPositionFlag=${this._firstPositionFlag}`);
        this._possitionOffset--;
        return this._updateProgressBarPositions();
    }

    private _updateProgressBarPositions(): boolean {
        this._possitionOffset = Math.max(0, this._possitionOffset);
        // this._possitionOffset = Math.min(this._configRepo.featureProgressbarPostions, this._possitionOffset);
        // console.log(`this._possitionOffset=${this._possitionOffset}`);
        this.progressBarPositionsArr;
        return true;
    }

    get onLeftArrowClick(): boolean {
        if (this.isInvisibleLeftArrow)
            return false

        // console.log(`clicked on onLeftArrowClick`);
        // console.log(`2. this._firstPositionFlag=${this._firstPositionFlag}`);
        this._possitionOffset++;
        return this._updateProgressBarPositions();
    }

    get progressBarPositionsArr(): object[] {
        let positionsArr: number[] = Array.from(Array(this.maxpositionsCount).keys());
        let positionObjsArr: object[] = [];

        let firstPosition: number = positionsArr.length - this._possitionOffset - this._configRepo.featureProgressbarPostions;
        firstPosition = Math.max(0, firstPosition);
        let lastPostion: number = firstPosition + this._configRepo.featureProgressbarPostions;
        positionsArr = positionsArr.slice(firstPosition, lastPostion);
        this._firstPositionFlag = (firstPosition === 0) ? true: false;
        // console.log(`1. this._firstPositionFlag=${this._firstPositionFlag}`);

        for (let i of positionsArr) {
            let postionObj: object = {};
            postionObj['label'] = i + 1;
            switch (i + 1) {
                case 1:
                    postionObj['superscript'] = 'st';
                    break;
                case 2:
                    postionObj['superscript'] = 'nd';
                    break;
                case 3:
                    postionObj['superscript'] = 'rd';
                    break;
                default:
                    postionObj['superscript'] = 'th';
            }
            positionObjsArr.push(postionObj);
        }
        return positionObjsArr;
    }

    isInvisibleLabel(position: number): boolean {
        return position >= this._layoutService.currentSelectionsCount
    }

    get isInvisibleRightArrow(): boolean {
        return this._possitionOffset == 0;
    }

    get isInvisibleLeftArrow(): boolean {
        return this._firstPositionFlag == true;
    }
}