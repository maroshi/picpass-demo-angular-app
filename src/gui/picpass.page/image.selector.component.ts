/*
Copyright (c) 2021, Picpass solutions
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
* Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
* Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
* Neither the name of the Picpass solutions nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL Picpass solutions BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/
import { Component, HostListener, OnInit }    from '@angular/core';
import { PicturesRepository } from '../../dao/pictures.repository';
import { PictureRecord } from '../../dao/picture.model';
import { LayoutService, Layout } from '../../bom/layout.service';
import { ConfigRepository } from '../../dao/config.repository';
import { EventDispatchService } from '../../bom/events.dispatch.service';
import { AbstractSelector }  from './abstract.selector.class';
import { SecretKeywordsService } from '../../bom/secret.keywords.service';
import { ProgressBorderDirective } from '../common.components/progress.border.directive';

@Component({
    selector: 'image-selector-section',
    template: `
    <div id="pp-loadig-message">
        <img src="assets/icons/clock-animation-wait.gif"
             width="50%" height="50%">
        Loading assets...
    </div>
    <div pp-progress-border id="pp-images-selector" class="m-0"></div>
`}) 
export class ImageSelectorComponent extends AbstractSelector implements OnInit {
    private _currPicctureIdx: number = 0;
    private _picsArr: PictureRecord[] = null;
    private _maxRowsCount: number = 0;
    private _maxColumnsCount: number = 0;

    private _previousHoverOnImgElement: HTMLImageElement = null;
    private _previousHoverOnKeywordsElement: HTMLUListElement = null;

    private _selectionsCountInLayout: number = 0;

    constructor(public picturesRepo: PicturesRepository,
            public layoutService: LayoutService,
            public configRepo: ConfigRepository,
            public eventsServeice: EventDispatchService,
            public keywordService: SecretKeywordsService){
        super(picturesRepo,
              layoutService,
              configRepo,
              eventsServeice,
              keywordService);
              layoutService.layout = Layout.IMAGE;
    }

    public async ngOnInit() {
        await this.init();
    }

    public renderSelector(): void {
        this._renderImageSelector();
    }

    private _renderImageSelector() {
        const parentElm: HTMLElement = this.removeChildElements('pp-images-selector');
         // get the pictures layout arr
        this._initPicsLayoutArray();
        // dyanmically set the hight 
        const imageSelectorSectionHeight: number = this.claculateSelectorComponentHeight();
        const borderHeight: number = ProgressBorderDirective.borderWidth * 2;
        const imageMaxHeightPx: number = (imageSelectorSectionHeight - borderHeight) / this._maxRowsCount;
        const imageWidthPx: number = this.currentMaxWidthPx / this._maxColumnsCount;

        parentElm.style.height = `${imageSelectorSectionHeight}px`;
        // create maxRowsCount rows
        for (let currRow: number = 0; currRow < this._maxRowsCount; currRow++) {
            let rowHTMLElement: HTMLDivElement = this.createRowElement(currRow, imageMaxHeightPx);
            // in each row create maxColumnsCount figure elements
            for (let currFigure: number = 0; currFigure < this._maxColumnsCount; currFigure++) {
                let figureHTMLElement: HTMLDivElement = this.createFigureElement(currRow, currFigure, `${imageWidthPx}px`,  `${imageMaxHeightPx}px`);
                // create sub element img
                let imageHTMLElement: HTMLImageElement = document.createElement<any>('img');
                imageHTMLElement.id = `pp-image-${currRow}${currFigure}`;
                imageHTMLElement.className = `pp-img`;
                imageHTMLElement.src = this._pictureUrl;
                figureHTMLElement.appendChild(imageHTMLElement);

                let currPictureRecord: PictureRecord = this.layoutService.pictureRecordFromImgElement(imageHTMLElement);

                // create sub element ul keywords
                const keywordsArr: string[] = [];
                currPictureRecord.keywords.forEach(kwdRec => keywordsArr.push(kwdRec.name));
                let keywordsHTMLElement: HTMLUListElement = this.createKeywordsListElement(currRow,currFigure, keywordsArr);
                keywordsHTMLElement.classList.add(`pp-img-keywords`);
                // add event listeners for hovering over image
                if (this.configRepo.featureImageHover) {
                    imageHTMLElement.addEventListener('mouseenter', () => this.onImgHover(imageHTMLElement, keywordsHTMLElement));
                    keywordsHTMLElement.addEventListener('mouseleave', () => this.onImgLeave(imageHTMLElement, keywordsHTMLElement));
                }
                figureHTMLElement.appendChild(keywordsHTMLElement);

                figureHTMLElement.addEventListener('click', () => {
                    const currImgElement: HTMLImageElement = figureHTMLElement.querySelector('img');
                    const currPicRecord: PictureRecord = this.layoutService.pictureRecordFromImgElement(currImgElement);
                    this.onFigClick(currPicRecord)
                });
                rowHTMLElement.appendChild(figureHTMLElement);
                this._incrementCurrentPictureRecord();   
            }
            parentElm.appendChild(rowHTMLElement);
        }
    }

    public onFigClick(currPicRecord: PictureRecord): void {
        this.layoutService.handleSelectedPicture(currPicRecord);
        this._selectionsCountInLayout = this.layoutService.currentSelectionsCount % this.configRepo.initLayoutSelections;

        console.log(`selected ${this.layoutService.currentSelectionsCount} out of ${this.configRepo.initMaxSelections}. In layout selected ${this._selectionsCountInLayout} out of ${this.configRepo.initLayoutSelections}.   `);
        if (this.layoutService.currentSelectionsCount >= this.configRepo.initMaxSelections) {
            this.onLastSelection();
        }

        if (this._selectionsCountInLayout === 0) {
            // selected all the requried in current layout, generate a new layout
            this._renderImageSelector();
        }
    }

    public onImgLeave(imageHTMLElement: HTMLImageElement, keywordsHTMLElement: HTMLUListElement): void {
        // console.log(`on img leave captured`);
        imageHTMLElement.style.display = 'block';
        imageHTMLElement.classList.remove('pp-img-hover');
        keywordsHTMLElement.style.display = 'none';
        this._previousHoverOnImgElement = null;
    }

    public onImgHover(imageHTMLElement: HTMLImageElement, keywordsHTMLElement: HTMLUListElement): void {
        if (this._previousHoverOnImgElement != null && this._previousHoverOnImgElement?.id === imageHTMLElement.id)
            return; // no change still hovering over same img

        // trun off the previous keywords
        if (this._previousHoverOnKeywordsElement != null)
            this._previousHoverOnKeywordsElement.style.display = 'none' ;

        // turn on the previous img
        if (this._previousHoverOnImgElement != null)
            this._previousHoverOnImgElement.style.opacity = '1.0';
            
        imageHTMLElement.classList.add('pp-img-hover');
        keywordsHTMLElement.style.display = 'block';
        this._previousHoverOnKeywordsElement = keywordsHTMLElement;
        this._previousHoverOnImgElement = imageHTMLElement;
    }

    private _initPicsLayoutArray(): void {
        this._picsArr = this.layoutService.randomPicturesLayoutUniqueKeywords;
        this._currPicctureIdx = 0;
        this._maxRowsCount = this.layoutService.rows;
        this._maxColumnsCount = this.layoutService.colunms;
    }

    private get _pictureUrl(): string {
        const pictureId: number = this._currentPictureRecord.id;
        const retUrl: string = this.picturesRepo.getPictureDownloadUrlById(pictureId);
        return retUrl;
    }

    private _incrementCurrentPictureRecord(): PictureRecord {
        this._currPicctureIdx++;
        return this._picsArr[this._currPicctureIdx];
    }

    private get _currentPictureRecord(): PictureRecord {
        return this._picsArr[this._currPicctureIdx];
    }

    @HostListener('window:resize', ['$event'])
    public onResize(event) {
        console.log(`window.resize event captured.`);
        this.renderSelector();
    }

    public get targetHtmlElement(): HTMLElement {
        const liElmNodes: NodeListOf<HTMLLIElement> = document.querySelectorAll('li');
        const liElmArr: Array<HTMLLIElement> = Array.from(liElmNodes);
        const targetKeyword: string = this.layoutService.targetKeyword;
        const targetLiElement: HTMLLIElement = liElmArr.find(elem => elem.textContent === targetKeyword);
        const retImgElment: HTMLElement = targetLiElement
                                               .parentElement
                                               .parentElement
                                               .querySelector('img');
        return retImgElment;
    }
}