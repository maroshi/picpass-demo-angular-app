/*
Copyright (c) 2021, Picpass solutions
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
* Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
* Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
* Neither the name of the Picpass solutions nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL Picpass solutions BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/
import { EventDispatchService } from '../../bom/events.dispatch.service';
import { Layout, LayoutService } from '../../bom/layout.service';
import { ConfigRepository } from '../../dao/config.repository';
import { PicturesRepository } from '../../dao/pictures.repository';
import { Subscription } from 'rxjs';
import { SecretKeywordsService } from '../../bom/secret.keywords.service';
import { ProgressBorderDirective } from '../common.components/progress.border.directive';

export abstract class AbstractSelector {
    public eventSubscriptionsArr: Subscription[] = new Array<Subscription>(4);
    public currentMaxWidthPx: number = 0;

    constructor(public picturesRepo: PicturesRepository,
        public layoutService: LayoutService,
        public configRepo: ConfigRepository,
        public eventsServeice: EventDispatchService,
        public keywordsService: SecretKeywordsService){
          this.layoutService.initialLayout();
}

    public async init() {
        await this.picturesRepo.picturesSync();
        // console.log(`2. In ngOnInit have pictures arr sync at ${new Date().toISOString()}`);
        // console.log(`3. In ngOnInit init layout at ${new Date().toISOString()}`);
        await this.layoutService.init(this.picturesRepo.pictures);
        // remove the loading/waiting message element before height calculations
        document.getElementById('pp-loadig-message').style.display = 'none';
        // set progress border again (assuming config was registered by now)
        ProgressBorderDirective.EnableProgressBorder();

        // let maxImgWidth: string =  this.configRepo.getConfigObject('Initiation')['Picture:']['Max width'];
        // let maxImgHeight: string = this.configRepo.getConfigObject('Initiation')['Picture:']['Max height'];
        this.renderSelector();

        this.eventSubscriptionsArr[0] = this.eventsServeice.refreshLayoutEvent$
            .subscribe(msg => this._onRefreshLayout(msg));

        this.eventSubscriptionsArr[1] = this.eventsServeice.onHoverHint$
            .subscribe(msg => this.onHoverHint(msg));

        this.eventSubscriptionsArr[2] = this.eventsServeice.onLeaveHint$
            .subscribe(msg => this.onLeaveHint(msg));

        this.eventSubscriptionsArr[3] = this.eventsServeice.onToggleLayout$
            .subscribe(msg => this._onToggleLayout(msg));

        this.keywordsService.updateCurrentInitConfig();
    }

    private _onToggleLayout(msg: string): void {
        if (msg === 'initial undefined')
            return;
        // console.log(`${msg}`);
    }

    public onLeaveHint(msg: string): void {
        if (msg === 'initial undefined')
            return;
        // console.log(`${msg}`);
        const retImgElment: HTMLElement = this.targetHtmlElement;
        retImgElment.classList.remove('pp-target-img');
    }

    public onHoverHint(msg: string): void {
        if (msg === 'initial undefined')
            return;
        // console.log(`${msg}`);
        const retImgElment: HTMLElement = this.targetHtmlElement;
        retImgElment.classList.add('pp-target-img');
    }

    private _onRefreshLayout(msg: string) {
        if (msg === 'initial undefined')
            return;
        // console.log(msg);
        this.renderSelector();
    }

    public abstract renderSelector(): void

    abstract get targetHtmlElement(): HTMLElement;

    public claculateSelectorComponentHeight(): number {
        const viewPortHeight: number = document.querySelector('html').clientHeight;
        const titleSectionHeigth: number = document.getElementById('pp-title-section').getBoundingClientRect().height;
        this.currentMaxWidthPx = document.getElementById('pp-title-section').getBoundingClientRect().width;
        const hintSectionHeigth: number = document.getElementById('pp-hint-section').getBoundingClientRect().height;
        const porgressbarSectionHeigth: number = document.getElementById('pp-progressbar-section').getBoundingClientRect().height;
        const toolbarSectionHeigth: number = document.getElementById('pp-toolbar-section').getBoundingClientRect().height;
        const imageSelectorSectionHeight: number = viewPortHeight - titleSectionHeigth - hintSectionHeigth - porgressbarSectionHeigth - toolbarSectionHeigth - 20;
        return imageSelectorSectionHeight;
    }

    public onLastSelection() {
        //passed the max selection limit
        console.log(`reached max selections limit!`);
        if (this.configRepo.featureCloseOnCompletion) {
            this.eventsServeice.onClosePicpassWidget(`onClosePicpassWidget at: ${new Date().toISOString()}`);
        }
    }

    public removeChildElements(elementId: string): HTMLElement {
        const parentElm: HTMLElement = document.getElementById(elementId);
        if (parentElm == null)
            return;

        // when rebuild component clear all children
        while (parentElm.firstChild) {
            parentElm.removeChild(parentElm.lastChild);
        }
        return parentElm;
    }

    public createRowElement(rowNum: number, rowHeight: number): HTMLDivElement {
        let rowHTMLElement: HTMLDivElement = document.createElement<any>('div');
        rowHTMLElement.id = `pp-row-${rowNum}`;
        rowHTMLElement.className = `m-0 d-flex justify-content-center`;
        rowHTMLElement.style.height = `${rowHeight}px`;
        return  rowHTMLElement;
    }

    public createFigureElement(rowNum: number, colNum: number, width: string, height: string): HTMLDivElement {
        let figureHTMLElement: HTMLDivElement = document.createElement<any>('figure');
        figureHTMLElement.id = `pp-figure-${rowNum}${colNum}`;
        figureHTMLElement.className = `m-0 border border-secondary pp-figure`;
        figureHTMLElement.style.width = width;
        figureHTMLElement.style.height = height;
        return figureHTMLElement;
    }

    public createKeywordsListElement(row: number, col: number, keywordsArr: string[]): HTMLUListElement {
        let keywordsHTMLElement: HTMLUListElement = document.createElement<any>('ul');
        keywordsHTMLElement.id = `pp-keywords-${row}${col}`;
        keywordsHTMLElement.className = `pp-keywords list-unstyled pl-2`;
        for (let keywordStr of keywordsArr) {
            // append each keyword
            let keywordItemHTMLElement: HTMLUListElement = document.createElement<any>('li');
            keywordItemHTMLElement.innerText = keywordStr;
            keywordsHTMLElement.appendChild(keywordItemHTMLElement);
        }
        return keywordsHTMLElement;
    }
}
