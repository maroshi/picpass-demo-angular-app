/*
Copyright (c) 2021, Picpass solutions
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
* Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
* Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
* Neither the name of the Picpass solutions nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL Picpass solutions BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/
import { Component, HostListener, OnInit }    from '@angular/core';
import { PicturesRepository } from '../../dao/pictures.repository';
import { FrameRecord } from '../../dao/frame.model';
import { LayoutService, Layout } from '../../bom/layout.service';
import { ConfigRepository } from '../../dao/config.repository';
import { EventDispatchService } from '../../bom/events.dispatch.service';
import { AbstractSelector }  from './abstract.selector.class';
import { SecretKeywordsService } from '../../bom/secret.keywords.service';
import { ProgressBorderDirective } from '../common.components/progress.border.directive';

@Component({
    selector: 'text-selector-section',
    template: `
    <div id="pp-loadig-message">
        <img src="assets/icons/clock-animation-wait.gif"
             width="50%" height="50%">
        Loading assets...
    </div>
    <div pp-progress-border id="pp-text-selector" class="m-0"></div>
`}) 
export class TextSelectorComponent extends AbstractSelector implements OnInit {
    private _currPicctureIdx: number = 0;
    private _framesArr: FrameRecord[] = null;
    private _maxRowsCount: number = 0;
    private _maxColumnsCount: number = 0;

    private _previousHoverOnImgElement: HTMLImageElement = null;
    private _previousHoverOnKeywordsElement: HTMLUListElement = null;

    private _selectionsCountInLayout: number = 0;

    constructor(public picturesRepo: PicturesRepository,
            public layoutService: LayoutService,
            public configRepo: ConfigRepository,
            public eventsServeice: EventDispatchService,
            public keywordsService: SecretKeywordsService){
        super(picturesRepo,
              layoutService,
              configRepo,
              eventsServeice,
              keywordsService);
        layoutService.layout = Layout.TEXT;
    }

    public async ngOnInit() {
        await this.init();
    }

    public renderSelector(): void {
        this._renderTextSelector();
    }

    private _renderTextSelector() {
        const parentElm: HTMLElement = this.removeChildElements('pp-text-selector');
        // get the frames layout arr
        this._initFramesLayoutArray();
        // dyanmically set the hight 
        const textSelectorSectionHeight: number = this.claculateSelectorComponentHeight();
        const borderHeight: number = ProgressBorderDirective.borderWidth * 2;
        const frameMaxHeightPx: number = (textSelectorSectionHeight - borderHeight)/ this._maxRowsCount;

        parentElm.style.height = `${textSelectorSectionHeight}px`;
        // create maxRowsCount rows
        let framesCount: number = 0;
        for (let currRow: number = 0; currRow < this._maxRowsCount; currRow++) {
            let rowHTMLElement: HTMLDivElement = this.createRowElement(currRow, frameMaxHeightPx);

            // in each row create maxColumnsCount figure elements
            for (let currFigure: number = 0; currFigure < this._maxColumnsCount; currFigure++) {
                let figureHTMLElement: HTMLDivElement = this.createFigureElement(currRow, currFigure, `${100 / this._maxColumnsCount}%`, `${frameMaxHeightPx}px`);
                let keywordsHTMLElement: HTMLUListElement = this.createKeywordsListElement(currRow,currFigure, this._framesArr[framesCount].keywords);
                figureHTMLElement.appendChild(keywordsHTMLElement);
                figureHTMLElement.addEventListener('click', () => this.onFigClick(keywordsHTMLElement));
                rowHTMLElement.appendChild(figureHTMLElement);
                framesCount++;   
            }
            parentElm.appendChild(rowHTMLElement);
        }
    }

    public onFigClick(keywordsListElm: HTMLUListElement): void {
        const keywordsLiNodes: NodeListOf<HTMLLIElement> = keywordsListElm.querySelectorAll('li');
        const keywordsArr: string[] = new Array<string>();
        keywordsLiNodes.forEach(node => keywordsArr.push(node.textContent));
        this.layoutService.handleSelectedFrame(keywordsArr, keywordsListElm.parentElement.id.slice(10));

        this._selectionsCountInLayout = this.layoutService.currentSelectionsCount % this.configRepo.initLayoutSelections;

        console.log(`selected ${this.layoutService.currentSelectionsCount} out of ${this.configRepo.initMaxSelections}. In layout selected ${this._selectionsCountInLayout} out of ${this.configRepo.initLayoutSelections}.   `);
        if (this.layoutService.currentSelectionsCount >= this.configRepo.initMaxSelections) {
            this.onLastSelection();
        }

        if (this._selectionsCountInLayout === 0) {
            // selected all the requried in current layout, generate a new layout
            this._renderTextSelector();
        }
    }

    private _initFramesLayoutArray(): void {
        this._framesArr = this.layoutService.randomFramesLayoutUniqueKeywords;
        this._currPicctureIdx = 0;
        this._maxRowsCount = this.layoutService.rows;
        this._maxColumnsCount = this.layoutService.colunms;
    }

    public get targetHtmlElement(): HTMLElement {
        const liElmNodes: NodeListOf<HTMLLIElement> = document.querySelectorAll('li');
        const liElmArr: Array<HTMLLIElement> = Array.from(liElmNodes);
        const targetKeyword: string = this.layoutService.targetKeyword;
        const targetLiElement: HTMLLIElement = liElmArr.find(elem => elem.textContent === targetKeyword);
        const retImgElment: HTMLElement = targetLiElement
                                               .parentElement;
        return retImgElment;
    }    
    
    @HostListener('window:resize', ['$event'])
    public onResize(event) {
        console.log(`window.resize event captured.`);
        this.renderSelector();
    }

}