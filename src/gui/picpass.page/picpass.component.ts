/*
Copyright (c) 2021, Picpass solutions
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
* Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
* Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
* Neither the name of the Picpass solutions nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL Picpass solutions BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/
import { Component, OnInit }    from '@angular/core';
import { KeywordsRepository } from '../../dao/keywords.repository';
import { KeywordRecord } from '../../dao/keyword.model';
import { KeywordToPicCountRecord } from '../../dao/keywordToPicCount.model';
import { PictureRecord } from '../../dao/picture.model';
import { PicturesRepository } from '../../dao/pictures.repository';
import { LayoutService } from '../../bom/layout.service';
import { ConfigRepository } from 'src/dao/config.repository';


@Component({
    template: `
<div id="pp-picpass-page"
     class="mx-auto">
    <title-header id="pp-title-section" title="Picpass Demo"></title-header>

    <hint-section id="pp-hint-section"
                  [class.pp-removed]="!configRepo.featureHintPanel"></hint-section>

    <div *ngIf="layoutService.isTextLayout()">
        <text-selector-section id="pp-text-selector-section"></text-selector-section>
    </div>
    <div *ngIf="layoutService.isImageLayout()">
        <image-selector-section id="pp-image-selector-section"></image-selector-section>
    </div>

    <progressbar-section id="pp-progressbar-section"
                         [class.pp-removed]="!configRepo.featureProgressbarPanel"></progressbar-section>

    <toolbar-section id="pp-toolbar-section"></toolbar-section>
</div>
`})
export class PicpassComponent implements OnInit{
    public keywrdsArr: KeywordRecord[];
    public keywrdsArrLen: number = 0;
    public keywordsToPicCountsArr: KeywordToPicCountRecord[];
    public keywordsToPicCountsArrLen: number = 0;
    public pictursArr: PictureRecord[];
    public pictureRecordArrLen: number = 0;


    constructor(public keywordsRepo: KeywordsRepository,
                public picturesRepository: PicturesRepository,
                public layoutService: LayoutService,
                public configRepo: ConfigRepository){
    }

    ngOnInit() {
        // this.keywrdsArr = this.keywordsRepo.keywords;
        // console.log(`Inside ngOnInit()`);
        // console.dir(this.keywrdsArr);
        // this.keywrdsArrLen = this.keywrdsArr.length;
        // console.log(`this.keywordsRepo.keywords=${this.keywordsRepo.keywords}`);
        const picpassElement: HTMLElement = document.getElementById('pp-picpass-page');
        picpassElement.style.maxWidth = `${this.configRepo.initMaxWidth}px`;
    }

    get keywordsCount(): number {
        this.keywrdsArr = this.keywordsRepo.keywords;
        if (typeof this.keywrdsArr !== 'undefined')
            this.keywrdsArrLen = this.keywrdsArr.length;
        return this.keywrdsArrLen;
    }

    get keywordsToPicsCount(): number {
        this.keywordsToPicCountsArr = this.keywordsRepo.keywordsToPicCounts;
        if (typeof this.keywordsToPicCountsArr !== 'undefined')
            this.keywordsToPicCountsArrLen = this.keywordsToPicCountsArr.length;
        return this.keywordsToPicCountsArrLen;
    }

    get picturesCount (): number {
        this.pictursArr = this.picturesRepository.pictures;
        if (typeof this.pictursArr !== 'undefined')
            this.pictureRecordArrLen = this.pictursArr.length;
        return this.pictureRecordArrLen;
    }
}
