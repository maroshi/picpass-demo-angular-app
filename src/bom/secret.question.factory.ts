/*
Copyright (c) 2021, Picpass solutions
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
* Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
* Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
* Neither the name of the Picpass solutions nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL Picpass solutions BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

class QuestionTemplateFactory {
  private _currentTemplateIdx: number = 0;
  private _templates: string[] = [];

  constructor(templates: string[]) {
    this._templates = templates;
  }

  public get currentTemplate() {
    const template: string = this._templates[this._currentTemplateIdx];
    this._currentTemplateIdx = (this._currentTemplateIdx + 1) % this._templates.length;
    return template;
  }
}

export class SecretQuestionFactory {
  // template alligned to KeywordCategoryType
  private static _templates: QuestionTemplateFactory[] = [
    // noun
    new QuestionTemplateFactory(['Find the |.',
                                 'Where is a/an |?',
                                 'Idenitfy picture with a/an |.',
                                 'Picture with a/an |.']),
    // verb
    new QuestionTemplateFactory(['Something that is |ed.',
                                 'Identify a/an |ed object.',
                                 'Where is the |ing object?',
                                 'Find a/an |ing object.']),
    // descr
    new QuestionTemplateFactory(['Something that is |.',
                                 'Identify a/an | object.',
                                 'Where is the | object?',
                                 'Find a/an | object.',
                                 'What is |?'])
  ]

  public static  getQuestionTempalteForSecretKeywordCategoryType(categoryType: number): string[] {
    const currentTemplate: string = this._templates[categoryType].currentTemplate;
    return currentTemplate.split('|');
  }
}
