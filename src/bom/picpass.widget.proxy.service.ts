/*
Copyright (c) 2021, Picpass solutions
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
* Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
* Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
* Neither the name of the Picpass solutions nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL Picpass solutions BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/
import { DefaultIterableDiffer, Injectable }    from '@angular/core';
import { ConfigRepository } from 'src/dao/config.repository';
import { ConfigDataSource} from '../dao/config.datasource';

@Injectable()
export class PicpassWidgetProxyService {
    private _msgHistory: string[] = new Array<string>();
    private _parentWindos: Window;
    private _initialPasswordValue: string = '';

    constructor(private _configDataSource: ConfigDataSource,
                private _configRepo: ConfigRepository) {
      this._parentWindos = window.parent;
      this.setInitialPasswordValue();

      this._parentWindos.postMessage({
        "topic": "register",
        "message": {}
      },"*");

      window.addEventListener('message', (event: any) => {
          // console.log('child received message!:  ',event.data);
          let messageTopic: string = event.data.topic;
          switch (messageTopic) {
              case 'register' :
                  // console.log(`Ackonwledge: handling register picpass page`, event.data.message);
                  if (Object.keys(event.data.message).length === 0)
                    break;
                  this._configDataSource.data = event.data.message;
                  this._configRepo.reconstruct();
                  this.setInitialPasswordValue();
                  break;
              case 'apply-password-text' :
                  console.log(`Ackonwledge: handling apply password text`, event.data.message);
                  break;
              }
      });
    }

    public setInitialPasswordValue() {
      this._initialPasswordValue = this._configRepo.initPasswordInitalValue;
      if (this._configRepo.featurePasswordValueReset || typeof this._initialPasswordValue === 'undefined')
        this._initialPasswordValue = "";

      this._msgHistory = new Array<string>();
      this._msgHistory.push(this._initialPasswordValue);
    }

    public cancelDialog() {
        this.rewindPasswordText(1);
        this.closeDialog();
    }

    private _postMessage(msgObj: { topic: string; message: {}; }, arg1: string): void {
        // console.dir(msgObj);
        // console.log(`posteMessage at: ${new Date().toISOString()}`);
        this._parentWindos.postMessage(msgObj,"*");
        console.log(`posted message: ${msgObj.message},${msgObj.topic}`);
    }

    public closeDialog() {
        this._postMessage({
            "topic": "dialog-close",
            "message": {}
        },"*");
    }

    public submitDialog() {
      this._postMessage({
          "topic": "submit-password",
          "message": {}
      },"*");
    }

    public applyPasswordText(inText: string) {
        let latestText = inText;
        if (this._msgHistory.length > 0)
            latestText = this._msgHistory[this._msgHistory.length - 1] + inText;

        this._postMessage({
            "topic": "apply-password-text",
            "message": latestText
        },"*");
        this._msgHistory.push(latestText);
    }

    public rewindPasswordText(textIdx: number) {
        if (textIdx == 1) {
            this._msgHistory = new Array<string>();
            this._msgHistory.push(this._initialPasswordValue);
        } else {
            this._msgHistory = this._msgHistory.slice(0, textIdx - 1);
        }

        this._postMessage({
            "topic": "apply-password-text",
            "message": this._msgHistory[this._msgHistory.length - 1]
        },"*");
    }
}
