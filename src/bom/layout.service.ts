/*
Copyright (c) 2021, Picpass solutions
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
* Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
* Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
* Neither the name of the Picpass solutions nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL Picpass solutions BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/
import { Injectable }    from '@angular/core';
import { ConfigRepository } from '../dao/config.repository';
import { PictureRecord } from '../dao/picture.model';
import { FrameRecord } from '../dao/frame.model';
import { PicpassWidgetProxyService } from './picpass.widget.proxy.service';
import { PictureUsageRecord } from './pictures.usage.record.class';
import { EventDispatchService } from './events.dispatch.service';

@Injectable()
export class LayoutService {
    private _picturesArr: PictureRecord[];
    private _framesArr: FrameRecord[];
    private _targetPicIdx: number = -1;
    private _layoutPicsArr: PictureRecord[] = null;
    private _passwordSegmentsArr: string[];
    private _keywordsArr: string[] = null;
    private _currentSelectionsCount: number = 0;
    private _maxPicutreLookupIterations: number = 1000000;
    private _picturesUsageMap: Map<string, PictureUsageRecord> = new Map();

    private _layout: Layout = Layout.IMAGE;

    constructor(private _configRep: ConfigRepository,
                private _picpassProxyService: PicpassWidgetProxyService,
                private _eventsService: EventDispatchService){
        this.initialLayout();
    }

    public get rows(): number {
        return this._configRep.initRows;
    }

    public get colunms(): number {
        return this._configRep.initColumns;
    }

    public get layoutArrSize(): number {
        return this.colunms * this.rows;
    }

    public get targetPictureRecord(): PictureRecord {
        if (this._layoutPicsArr == null)
            return null;
        return this._layoutPicsArr[this._targetPicIdx];
    }

    public get currentSelectionsCount(): number {
        return this._currentSelectionsCount;
    }

    public set currentSelectionsCount(n: number) {
        this._currentSelectionsCount = n;
        this._eventsService.onChangedSecretKeyword(this.targetKeyword);
    }

    public get targetKeyword(): string {
        if (this._keywordsArr == null)
            this._keywordsArr = this._configRep.initKeywords;
        return this._keywordsArr[this._currentSelectionsCount % this._configRep.initMaxSelections];
    }


    public init(picsArr: PictureRecord[]) {
        let config: ConfigRepository = this._configRep;
        this._passwordSegmentsArr = this._segmentPassword(config.initPassword);
        this._keywordsArr = config.initKeywords;
        this._picturesArr = picsArr;
        this._targetPicIdx = 0;
        this.currentSelectionsCount = 0;

        console.log(`1. Init a layout of ${this.rows * this.colunms} out of ${this._picturesArr.length} picutres. At ${new Date().toISOString()}`);
    }

    private _segmentPassword(pswd: string): string[] {
        let segmentsLenghtsArr: number[] = Array<number>(this._configRep.initMaxSelections);
        const smallSegmentSize: number = Math.floor(pswd.length / this._configRep.initMaxSelections);
        const largeSegmentSize: number = Math.ceil(pswd.length / this._configRep.initMaxSelections);
        const largeSegmentsCount: number = pswd.length % this._configRep.initMaxSelections;
        const smallSegmentsCount: number = this._configRep.initMaxSelections - largeSegmentsCount;
        segmentsLenghtsArr.fill(largeSegmentSize, 0, largeSegmentsCount);
        segmentsLenghtsArr.fill(smallSegmentSize, largeSegmentsCount, this._configRep.initMaxSelections);
        // insert grain of salt/iregularity
        if (this._configRep.initMaxSelections > 2 && smallSegmentSize > 0) {
            segmentsLenghtsArr[0]--;
            segmentsLenghtsArr[1]++;
        }
        // shufle segmentsLenghtsArr
        for (let i = 0; i < segmentsLenghtsArr.length / 2; i ++) {
            const replaceTargetInterval: number = LayoutService.getRandomIntInRange(1, segmentsLenghtsArr.length);
            const replaceTargetIdx: number = (i + replaceTargetInterval) % segmentsLenghtsArr.length;
            const tempSegmentSize: number = segmentsLenghtsArr[i];
            segmentsLenghtsArr[i] = segmentsLenghtsArr[replaceTargetIdx];
            segmentsLenghtsArr[replaceTargetIdx] = tempSegmentSize;
        }
        // console.log(`segmentsLenghtsArr=${segmentsLenghtsArr.toString()}`);

        let passwordSegmentsArr = new Array<string>();
        let positionInPasswrdStr: number = 0;
        for(const segmentLength of segmentsLenghtsArr) {
            const currentPasswordSegment: string = this._configRep.initPassword.slice(positionInPasswrdStr, positionInPasswrdStr + segmentLength);
            passwordSegmentsArr.push(currentPasswordSegment);
            positionInPasswrdStr += segmentLength;
        }
        // console.log(`passwordSegmentsArr=${passwordSegmentsArr.toString()}`);
        return passwordSegmentsArr;
    }

    public get randomLayout(): PictureRecord[] {
        let layoutSet: Set<PictureRecord> = new Set();
        while (layoutSet.size < this.layoutArrSize) {
            let currPicIdx: number = LayoutService.getRandomIntInRange(0, this._picturesArr.length);
            let currPicRecrod: PictureRecord = this._picturesArr[currPicIdx];

            if (typeof currPicRecrod === 'undefined' || currPicRecrod.keywords.length === 0)
                continue;

            layoutSet.add(currPicRecrod);
        }
        this._layoutPicsArr = Array.from(layoutSet);
        this._targetPicIdx = LayoutService.getRandomIntInRange(0, (this.layoutArrSize - 1));
        return this._layoutPicsArr;
    }

    private _getPicRecordForKeyword(currentSecretKeyword: string): PictureRecord {
        let usageHistoryRecord: PictureUsageRecord;
        usageHistoryRecord = this._picturesUsageMap.get(currentSecretKeyword);
        if (typeof usageHistoryRecord === 'undefined') {
            // build a usage history Record for this keyword
            let picturesRelatedToKeywordArr: PictureRecord[];
            picturesRelatedToKeywordArr = this._picturesArr.filter(currPicRecord => {
                let foundKeyword: number = currPicRecord.keywords.findIndex(keyword => keyword.name === currentSecretKeyword);
                return foundKeyword > -1;
            });
            usageHistoryRecord = new PictureUsageRecord(picturesRelatedToKeywordArr);
            this._picturesUsageMap.set(currentSecretKeyword, usageHistoryRecord);
        }
        // get least used picture
        let retPicRecord: PictureRecord = usageHistoryRecord.getNextPicture();
        return retPicRecord;
    }

    public get randomPicturesLayoutUniqueKeywords(): PictureRecord[] {
        let layoutSet: Set<PictureRecord> = new Set();
        let keywordsSet: Set<string> = new Set();
        let loopIteratinos: number = 0;
        let secretKeywordsCount: number = 0;

        // collect pictures with secret keywords
        while (layoutSet.size < this._configRep.initLayoutSelections && loopIteratinos < this._maxPicutreLookupIterations) {
            const currentKeywordIdx: number = (this._currentSelectionsCount + secretKeywordsCount) % this._configRep.initMaxSelections;
            const currentSecretKeyword: string = this._keywordsArr[currentKeywordIdx];
            const currPicRecrod: PictureRecord = this._getPicRecordForKeyword(currentSecretKeyword);
            loopIteratinos++;

            if (typeof currPicRecrod === 'undefined' || currPicRecrod.keywords.length === 0)
                continue;
            if (this._isPictureKeywordsExistInSet(keywordsSet, currPicRecrod))
                continue;

            currPicRecrod.keywords.forEach(currentKeywordRecord => {
                keywordsSet.add(currentKeywordRecord.name);
            });

            layoutSet.add(currPicRecrod);
            secretKeywordsCount++;
        }

        loopIteratinos = 0;
        // collect rest of pictures
        while (layoutSet.size < this.layoutArrSize && loopIteratinos < this._maxPicutreLookupIterations) {
            const currPicIdx: number = LayoutService.getRandomIntInRange(0, this._picturesArr.length);
            const currPicRecrod: PictureRecord = this._picturesArr[currPicIdx];
            loopIteratinos++;

            if (typeof currPicRecrod === 'undefined' || currPicRecrod.keywords.length === 0)
                continue;
            if (this._isPictureKeywordsExistInSet(keywordsSet, currPicRecrod))
                continue;

            currPicRecrod.keywords.forEach(currentKeywordRecord => {
                keywordsSet.add(currentKeywordRecord.name);
            });

            layoutSet.add(currPicRecrod);
        }

        // shuffle pictures array
        this._layoutPicsArr = this._shufflePicturesArray(Array.from(layoutSet));
        return this._layoutPicsArr;
    }

    public get randomFramesLayoutUniqueKeywords(): FrameRecord[] {
        let keywordsSet: Set<string> = new Set();
        let secretKeywordsCount: number = 0;

        // collect secret keywords into set
        while (keywordsSet.size < this._configRep.initLayoutSelections) {
            const currentKeywordIdx: number = (this._currentSelectionsCount + secretKeywordsCount) % this._configRep.initMaxSelections;
            const currentSecretKeyword: string = this._keywordsArr[currentKeywordIdx];
            if (keywordsSet.has(currentSecretKeyword)) {
                console.log(`Cant have same secret keywords ${currentSecretKeyword} in a same text frame.`);
                return null;
            }
            keywordsSet.add(currentSecretKeyword);
            secretKeywordsCount++;
        }

        // collect other keywords into set
        while (keywordsSet.size < this.layoutArrSize * this._configRep.initFrameMaxKeywords) {
            const currPicIdx: number = LayoutService.getRandomIntInRange(0, this._picturesArr.length);
            const currPicRecrod: PictureRecord = this._picturesArr[currPicIdx];

            if (typeof currPicRecrod === 'undefined' || currPicRecrod.keywords.length === 0)
                continue;
            currPicRecrod.keywords.forEach((keywordRec) => keywordsSet.add(keywordRec.name));
        }

        // sort keywords on keyword ends
        let sortedKeywordsArr = Array.from(keywordsSet);
        // reverse keywords
        // sortedKeywordsArr = sortedKeywordsArr.map(keywordStr => keywordStr.split("").reverse().join(""));
        sortedKeywordsArr = sortedKeywordsArr.sort();
        // reverse keywords again
        // sortedKeywordsArr = sortedKeywordsArr.map(keywordStr => keywordStr.split("").reverse().join(""));

        // allocate keywords into frames
        this._framesArr = new Array<FrameRecord>();
        for (let i: number = 0; i < this.layoutArrSize; i++) {
            let currFrameRecord: FrameRecord = new FrameRecord();
            currFrameRecord.keywords = new Array<string>();
            for(let q: number = 0; q < this._configRep.initFrameMaxKeywords; q++) {
                const currentKeyword: string = sortedKeywordsArr[(i * this._configRep.initFrameMaxKeywords) + q];
                currFrameRecord.keywords.push(currentKeyword);
            }
            this._framesArr.push(currFrameRecord);
        }
        return this._framesArr;
    }

    public handleSelectedFrame(keywordsArr: string[], frameId: string): void {
        let currentPasswordSegment = `☹️[${frameId}]`;
        const correctSelectionIdx = keywordsArr.findIndex(keyword => keyword === this.targetKeyword);
        if (correctSelectionIdx > -1) {
            const currentPasswordSegmentIdx: number = this.currentSelectionsCount % this._configRep.initMaxSelections;
            currentPasswordSegment = this._passwordSegmentsArr[currentPasswordSegmentIdx];
        }

        this.currentSelectionsCount++;
        // console.log(`currentPasswordSegment=${currentPasswordSegment}`);
        this._picpassProxyService.applyPasswordText(currentPasswordSegment);
    }

    private _shufflePicturesArray(inArray: Array<PictureRecord>): Array<PictureRecord> {
        for (let i = 0; i < inArray.length / 2; i ++) {
            const replaceTargetInterval: number = LayoutService.getRandomIntInRange(1, inArray.length);
            const replaceTargetIdx: number = (i + replaceTargetInterval) % inArray.length;
            const tempPicRecord: PictureRecord = inArray[i];
            inArray[i] = inArray[replaceTargetIdx];
            inArray[replaceTargetIdx] = tempPicRecord;
        }
        return inArray;
    }

    private _isPictureKeywordsExistInSet(keywrodsSet: Set<string>, picRecord: PictureRecord) {
        let foundKeyword = picRecord.keywords.findIndex((currKeywordRecord) => {
            let foundIt: boolean = keywrodsSet.has(currKeywordRecord.name);
            // console.log(`keywrodsSet.has(${currKeywordRecord.name}) = ${foundIt}`)
            return foundIt;
        });
        // console.log(`foundKeyword=${foundKeyword}`)
        return foundKeyword > -1;
    }

    public static getRandomIntInRange(minValArg: number, maxValArg: number): number {
        const minIntVal = Math.floor(minValArg);
        const intRange = Math.floor(maxValArg - minIntVal + 1);
        const retval = Math.floor(Math.random() * intRange) + minIntVal;
        return retval;
    }

    public pictureRecordFromImgElement(imgElm: HTMLImageElement): PictureRecord {
        const currSrc: string = imgElm.src;
        const idPosInSrc: number = currSrc.search('[0-9]+$');
        const currPicId: number = Number(currSrc.slice(idPosInSrc));
        const currPicIdx: number = this._picturesArr.findIndex(picRecord => picRecord.id == currPicId);
        const retPictureRecord: PictureRecord = this._picturesArr[currPicIdx];
        return retPictureRecord;
    }

    public handleSelectedPicture(currPicRecord: PictureRecord) {
        let currentPasswordSegment = `☹️<${currPicRecord.id}>`;
        const correctSelectionIdx = currPicRecord.keywords.findIndex(keyword => keyword.name === this.targetKeyword);
        if (correctSelectionIdx > -1) {
            const currentPasswordSegmentIdx: number = this.currentSelectionsCount % this._configRep.initMaxSelections;
            currentPasswordSegment = this._passwordSegmentsArr[currentPasswordSegmentIdx];
        }

        this.currentSelectionsCount++;
        // console.log(`currentPasswordSegment=${currentPasswordSegment}`);
        this._picpassProxyService.applyPasswordText(currentPasswordSegment);
    }

    public isTextLayout() {return this._layout === Layout.TEXT}

    public isImageLayout() {return this._layout === Layout.IMAGE}

    public set layout(val: Layout) {this._layout = val}

    public get layout(): Layout {return this._layout}

    public initialLayout() {
      this.layout = Layout.TEXT
      if (this._configRep.featureImageSelections)
          this.layout = Layout.IMAGE;
    }
}

export enum Layout {
    IMAGE,
    TEXT
}
