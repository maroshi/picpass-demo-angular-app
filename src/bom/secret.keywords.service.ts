/*
Copyright (c) 2021, Picpass solutions
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
* Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
* Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
* Neither the name of the Picpass solutions nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL Picpass solutions BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/
import { Injectable }    from '@angular/core';
import { KeywordsRepository } from 'src/dao/keywords.repository';
import { KeywordToPicCountRecord } from 'src/dao/keywordToPicCount.model';
import { ConfigRepository } from '../dao/config.repository';
import { LayoutService } from './layout.service';
import { QuestionsRepository } from '../dao/questions.repository';
import { QuestionRecord } from 'src/dao/question.model';

@Injectable()
export class SecretKeywordsService {
    private _keywords2PicCountsArr: KeywordToPicCountRecord[];
    private _kewordsMapByCategory: Map<string, string[]> = new Map();
    private _keywordsArrArr: Array<string[]> = new Array();
    private _minPicsForKeyword: number = 10;
    private _categoriesForSecretPhrase: {} = {
        "2": ["noun","verb",],
        "3": ["descr","noun","verb"],
        "4": ["verb","noun","descr","noun"],
        "5": ["verb","noun","descr","noun", "verb"],
        "6": ["descr","noun","verb","descr","noun","verb"],
        "7": ["descr","noun","verb","descr","noun","verb","noun"],
        "8": ["descr","noun","verb","descr","noun","verb","noun","verb"],
        "9": ["descr","noun","verb","descr","noun","verb","noun", "verb","descr"]
    }
    private _firstTimeFlag: boolean = true;
    private _currentInitConfig: any;
    public InitiatedFlag: boolean = false;

    public get currentInitConfig(): object {
        return this._currentInitConfig;
    }

    public updateCurrentInitConfig() {
        this._currentInitConfig = ConfigRepository.deepClone<any>(this._configRep.initObj);
    }

    public logCurrentInitConifg(prefix: string) {
        console.log(`${prefix} _currentInitConfig=${this._currentInitConfig["Keywords:"]}`);
    }

    public get keywordsArrArr(): Array<string[]> {return this._keywordsArrArr}

    public set keywordsArrArr(kwdArrArr: Array<string[]>) {this._keywordsArrArr = kwdArrArr}
    
    constructor(private _configRep: ConfigRepository,
                private _kewordsRepo: KeywordsRepository,
                private _quesetionsRep: QuestionsRepository){
        this.updateCurrentInitConfig();
    }

    public keywordsCategoryType: string[] = [
        'noun', 
        'verb', 
        'descr', 
        'place', 
        'number', 
        'person', 
        'private'
      ];

      public categoryTypeByKeyword(keyword: string): number {
        const category: string = this.getCategoryForKeyword(keyword);
        const categoryType: string[] = this.keywordsCategoryType;
        let   matchedCategoryType: number = -1;
        switch (category) {
          case categoryType[0]:
            matchedCategoryType = 0;
            break;
          case categoryType[1]:
            matchedCategoryType = 1;
            break;
          case categoryType[2]:
            matchedCategoryType = 2;
            break;
          case categoryType[3]:
            matchedCategoryType = 3;
            break;
          case categoryType[4]:
            matchedCategoryType = 4;
            break;
          case categoryType[5]:
            matchedCategoryType = 5;
            break;
          case categoryType[6]:
            matchedCategoryType = 6;
            break;
    
          default:
            matchedCategoryType = -1;
          }
        return matchedCategoryType;
      }
    

    private _prepareKeywordsArraysInList(initMaxSelections: number): Array<string[]> {
      const categoriesArr: string[] = this.getCategoriesForSecretPhrase(initMaxSelections);
      for (let i: number  = 0; i < categoriesArr.length; i ++) {
        const currCategory: string = categoriesArr[i];
        const currKeywordsArr: string[] = this.getKeywordsForCategory(currCategory);
        this._keywordsArrArr.push(currKeywordsArr);
      }
      return this._keywordsArrArr;
    }

    public async init() {
        if (this.InitiatedFlag)
          return;

        this._keywords2PicCountsArr = await this._kewordsRepo.keywordsToPicCountsSync();

        this._keywords2PicCountsArr.forEach(kwd2picCntRec => {
            if (this._kewordsMapByCategory.has(kwd2picCntRec.category) === false) {
                let keywordsArr = new Array<string>();
                this._kewordsMapByCategory.set(kwd2picCntRec.category, keywordsArr);
            }
            let keywordsArr: string[] = this._kewordsMapByCategory.get(kwd2picCntRec.category);
            if (kwd2picCntRec.picsCount >= this._minPicsForKeyword)
                keywordsArr.push(kwd2picCntRec.name);
        });
        // read question records async now, when arrive add to _kewordsMapByCategory
        this._quesetionsRep.charachterQuestions;
        this._quesetionsRep.numberQuestions;
        this._quesetionsRep.placesQuestions;
        this._quesetionsRep.privateQuestions;

        this.InitiatedFlag = true;
    }

    public addQuestionsCategoryToKeywordsMap(categoryName: string, questionsArr: QuestionRecord[]): void {
        const keywordsStrArr: string[] = []
        questionsArr.forEach(questionRec => keywordsStrArr.push(questionRec.text));
        this._kewordsMapByCategory.set(categoryName, keywordsStrArr);
    }

    public getCategoriesForSecretPhrase(keywordsCount: number): string[] {
        const categoriesArr: string[] = this._categoriesForSecretPhrase[keywordsCount.toString()];
        return categoriesArr;
    }

    public getKeywordsForCategory(category: string): string[] {
        const keywordsArr: string[] = this._kewordsMapByCategory.get(category);
        return keywordsArr;
    }

    public getCategoryForKeyword(kwd: string): string {
        const foundK2pRecIdx: number = this._keywords2PicCountsArr?.findIndex(k2pRec => k2pRec.name === kwd);
        if (foundK2pRecIdx > -1)
            return this._keywords2PicCountsArr[foundK2pRecIdx].category;
        // dealing with questions text. need to looup on by one.
        for (let i: number = this.keywordsCategoryType.length - 1; i > 2; i--) {
            const currentCategory: string = this.keywordsCategoryType[i];
            const keywordsInCurrCategory: string[] = this._kewordsMapByCategory.get(currentCategory);
            const foundKeywordIdx: number = keywordsInCurrCategory.findIndex(keyword => keyword === kwd);
            if (foundKeywordIdx > -1)
                return currentCategory;
        }
    }

    public getRandomKeywordFromCategory(category: string) {
        const keywordsArrInCategory: string[] = this.getKeywordsForCategory(category);
        const aKeywordIdx = LayoutService.getRandomIntInRange(0, keywordsArrInCategory.length - 1);
        return keywordsArrInCategory[aKeywordIdx];
    }

    public shuffleSecretKeywords() {
        let secretKeywordsArr: string[] = this._configRep.initKeywords.slice(0, this._configRep.initMaxSelections);
        secretKeywordsArr.forEach((keyword, i) => {
            let currentCategory: string = this.getCategoryForKeyword(keyword);
            if (this._firstTimeFlag) {
                const initialCategoriesArr: string[] = this.getCategoriesForSecretPhrase(this._configRep.initMaxSelections);
                currentCategory = initialCategoriesArr[i];
            }
            this._configRep.initKeywords[i] = this.getRandomKeywordFromCategory(currentCategory);
          });
          this._firstTimeFlag = false;
    }

    public set firstTimeFlag(flag: boolean) {this._firstTimeFlag = flag}
}
