/*
Copyright (c) 2021, Picpass solutions
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
* Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
* Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
* Neither the name of the Picpass solutions nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL Picpass solutions BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/
import { Injectable }    from '@angular/core';
import { BehaviorSubject } from 'rxjs';


@Injectable()
export class EventDispatchService {
    private _refreshMessageSubject = new BehaviorSubject<string>('initial undefined');
    public refreshLayoutEvent$ = this._refreshMessageSubject.asObservable();

    public onRefreshLayout(msg: string) {
        this._refreshMessageSubject.next(msg);
    }

    private _hoverHintSubject = new BehaviorSubject<string>('initial undefined');
    public onHoverHint$ = this._hoverHintSubject.asObservable();

    public onHoverHint(msg: string) {
        this._hoverHintSubject.next(msg);
    }

    private _leaveHintSubject = new BehaviorSubject<string>('initial undefined');
    public onLeaveHint$ = this._leaveHintSubject.asObservable();

    public onLeaveHint(msg: string) {
        this._leaveHintSubject.next(msg);
    }

    private _toggleLayoutSubject = new BehaviorSubject<string>('initial undefined');
    public onToggleLayout$ = this._toggleLayoutSubject.asObservable();

    public onToggleLayout(msg: string) {
        this._toggleLayoutSubject.next(msg);
    }

    private _updateKeywordsCategories = new BehaviorSubject<string>('initial undefined');
    public onUpdateKeywordsCategories$ = this._updateKeywordsCategories.asObservable();

    public onUpdateKeywordsCategories(msg: string) {
        this._updateKeywordsCategories.next(msg);
    }

    private _submitSecretKeywords = new BehaviorSubject<string>('initial undefined');
    public onSubmitSecretKeywords$ = this._submitSecretKeywords.asObservable();

    public onSubmitSecretKeywords(msg: string) {
        this._submitSecretKeywords.next(msg);
    }

    private _changedSecretKeyword = new BehaviorSubject<string>('initial undefined');
    public onChangedSecretKeyword$ = this._changedSecretKeyword.asObservable();

    public onChangedSecretKeyword(msg: string) {
        this._changedSecretKeyword.next(msg);
    }

    private _closePicpassWidget = new BehaviorSubject<string>('initial undefined');
    public onClosePicpassWidget$ = this._closePicpassWidget.asObservable();

    public onClosePicpassWidget(msg: string) {
        this._closePicpassWidget.next(msg);
    }
}
