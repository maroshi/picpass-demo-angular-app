/*
Copyright (c) 2021, Picpass solutions
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
* Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
* Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
* Neither the name of the Picpass solutions nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL Picpass solutions BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/
import { Injectable, OnInit } from "@angular/core";
import { ConfigDataSource } from './config.datasource';
import { KeywordsDataSource } from './keywords.datasource';
import { KeywordRecord } from './keyword.model';
import { KeywordToPicCountRecord } from './keywordToPicCount.model';


@Injectable()
export class KeywordsRepository {
    private _keywordsArr: KeywordRecord[];
    private _keywordsToPicCountsArr: KeywordToPicCountRecord[];

    constructor(private _config: ConfigDataSource,
                private _keywordsDataSource: KeywordsDataSource) {
    }

    get keywords(): KeywordRecord[] {
        // console.log(`inside get keywords()`);
        if (typeof this._keywordsArr === 'undefined') {
            console.log(`wait to receive keywords arr`);
            this._keywordsDataSource.getKeywords().then(data => {
                this._keywordsArr = data;
                // console.log(`Received 1 ${this._keywords.length} keywords`);
            });
        }
        return this._keywordsArr;
    }

    get keywordsToPicCounts(): KeywordToPicCountRecord[] {
        if (typeof this._keywordsToPicCountsArr === 'undefined') {
            console.log(`wait to receive keywordsToPics arr`);
            this._keywordsDataSource.getKeywordsToPicCounts().then(data => {
                this._keywordsToPicCountsArr = data;
            });
        }
        return this._keywordsToPicCountsArr;
    }

    public async keywordsToPicCountsSync(): Promise<KeywordToPicCountRecord[]> {
        console.log(`0. Enter keywordsToPicCountsSync() ${new Date().toISOString()}`);
        if (this._keywordsToPicCountsArr != null)
            return this._keywordsToPicCountsArr;

        const resp:KeywordToPicCountRecord[] = await this._keywordsDataSource.getKeywordsToPicCounts();
        this._keywordsToPicCountsArr = resp;
        // console.log(`1. Exit picturesSync() ${new Date().toISOString()}`);
        return this._keywordsToPicCountsArr;
    }
}