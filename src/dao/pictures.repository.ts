/*
Copyright (c) 2021, Picpass solutions
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
* Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
* Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
* Neither the name of the Picpass solutions nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL Picpass solutions BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/
import { Injectable, OnInit } from "@angular/core";
import { ConfigDataSource } from './config.datasource';
import { PicturesDataSource } from './pictures.datasource';
import { PictureRecord } from './picture.model';
import { KeywordRecord } from './keyword.model';


@Injectable()
export class PicturesRepository {
    private _picturesArr: PictureRecord[];
    private _baseUrl: string;

    public async picturesSync(): Promise<PictureRecord[]> {
        console.log(`0. Enter picturesSync() ${new Date().toISOString()}`);
        if (this._picturesArr != null)
            return this._picturesArr;

        const resp:PictureRecord[] = await this._picturesDataSource.getPictures();
        this._picturesArr = resp;
        // console.log(`1. Exit picturesSync() ${new Date().toISOString()}`);
        return this._picturesArr;
    }

    constructor(private _config: ConfigDataSource,
                private _picturesDataSource: PicturesDataSource) {
        this._baseUrl = this._picturesDataSource.url;
        // this.picturesSync();
        // console.log(`2. Test pictures arr at ${new Date().toISOString()} ${this._picturesArr}`);
    }


    public get pictures(): PictureRecord[] {
        return this._picturesArr;
    }

    public getById(idArg: number): PictureRecord{
        if (typeof this.pictures === 'undefined')
            return undefined;
        return this.pictures.filter(({ id }) => id === idArg )[0];
    }

    public getPictureDownloadUrlById(idArg: number): string {
        let currPicture: PictureRecord = this.getById(idArg);
        if (typeof currPicture === 'undefined') 
            return "";

        return `${this._baseUrl}/pictures/download/${idArg}`;
    }

    public getKeywordNamesArrById(idArg: number): string[] {
        let currPicture: PictureRecord = this.getById(idArg);
        if (typeof currPicture === 'undefined') 
            return [];

        let keywordsArr: KeywordRecord[] = currPicture.keywords;
        return Array.from<KeywordRecord, string>((keywordsArr), (k) => k.name);
     }
}