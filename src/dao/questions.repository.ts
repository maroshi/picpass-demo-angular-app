/*
Copyright (c) 2021, Picpass solutions
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
* Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
* Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
* Neither the name of the Picpass solutions nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL Picpass solutions BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/
import { Injectable, OnInit } from "@angular/core";
import { ConfigDataSource } from './config.datasource';
import { QuestionRecord } from './question.model';
import { QuestionsDataSource } from './questions.datasource';


@Injectable()
export class QuestionsRepository {
    private _charachterQuestionsArr: QuestionRecord[] = null;
    private _numbersQuestionsArr: QuestionRecord[] = null;
    private _placesQuestionsArr: QuestionRecord[] = null;
    private _privateQuestionsArr: QuestionRecord[] = null;
    private _baseUrl: string;

    private _isInitialized(obj: object): boolean {
        return obj != null && typeof obj !== 'undefined';
    }

    public async charachterQuestionsSync(): Promise<QuestionRecord[]> {
        console.log(`0. Enter charachterQuestionsSync() ${new Date().toISOString()}`);
        if (this._isInitialized(this._charachterQuestionsArr))
            return this._charachterQuestionsArr;

        const resp:QuestionRecord[] = await this._questionsDataSource.getCharachtersQuestions();
        this._charachterQuestionsArr = resp;
        console.log(`1. Exit charachterQuestionsSync() ${new Date().toISOString()}`);
        return this._charachterQuestionsArr;
    }

    public get charachterQuestions(): QuestionRecord[] {
        if (this._isInitialized(this._charachterQuestionsArr))
            return this._charachterQuestionsArr;

        this._questionsDataSource.getCharachtersQuestions().then(resp => this._charachterQuestionsArr = resp);
        return this._charachterQuestionsArr;
    }

    public async numberQuestionsSync(): Promise<QuestionRecord[]> {
        // console.log(`0. Enter numberQuestionsSync() ${new Date().toISOString()}`);
        if (this._isInitialized(this._numbersQuestionsArr))
            return this._numbersQuestionsArr;

        const resp:QuestionRecord[] = await this._questionsDataSource.getNumbersQuestions();
        this._numbersQuestionsArr = resp;
        return this._numbersQuestionsArr;
    }

    public get numberQuestions(): QuestionRecord[] {
        if (this._numbersQuestionsArr != null)
            return this._numbersQuestionsArr;

        this._questionsDataSource.getNumbersQuestions().then(resp => this._numbersQuestionsArr = resp);
        return this._numbersQuestionsArr;
    }

    public async placesQuestionsSync(): Promise<QuestionRecord[]> {
        // console.log(`0. Enter placesQuestionsSync() ${new Date().toISOString()}`);
        if (this._isInitialized(this._placesQuestionsArr))
            return this._placesQuestionsArr;

        const resp:QuestionRecord[] = await this._questionsDataSource.getPlacesQuestions();
        this._placesQuestionsArr = resp;
        return this._placesQuestionsArr;
    }

    public get placesQuestions(): QuestionRecord[] {
        if (this._placesQuestionsArr != null)
            return this._placesQuestionsArr;

        this._questionsDataSource.getPlacesQuestions().then(resp => this._placesQuestionsArr = resp);
        return this._placesQuestionsArr;
    }

    public async privateQuestionsSync(): Promise<QuestionRecord[]> {
        // console.log(`0. Enter privateQuestionsSync() ${new Date().toISOString()}`);
        if (this._privateQuestionsArr != null)
            return this._privateQuestionsArr;

        const resp:QuestionRecord[] = await this._questionsDataSource.getPrivateQuestions();
        this._privateQuestionsArr = resp;
        return this._privateQuestionsArr;
    }

    public get privateQuestions(): QuestionRecord[] {
        if (this._privateQuestionsArr != null)
            return this._privateQuestionsArr;

        this._questionsDataSource.getPrivateQuestions().then(resp => this._privateQuestionsArr = resp);
        return this._privateQuestionsArr;
    }

    constructor(private _config: ConfigDataSource,
                private _questionsDataSource: QuestionsDataSource) {
        this._baseUrl = this._questionsDataSource.url;
    }
}