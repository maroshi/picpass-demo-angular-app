/*
Copyright (c) 2021, Picpass solutions
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
* Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
* Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
* Neither the name of the Picpass solutions nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL Picpass solutions BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/
import { Injectable } from '@angular/core';
import { ConfigRepository } from './config.repository';
import { map } from 'rxjs/operators';
import { HttpClient } from '@angular/common/http';
import { QuestionRecord } from './question.model';


@Injectable()
export class QuestionsDataSource {
    private _url: string;

    constructor(private _configRepo: ConfigRepository,
                private _httpClient: HttpClient) {
        this._url = this._configRepo.url;
    }

    public getCharachtersQuestions(): Promise<QuestionRecord[]> {
        return this._httpClient.get<QuestionRecord[]>(`${this._url}/questionCategories/1/questions`)
        .pipe(
            map(data => data['_embedded'].questions)
        )
        .toPromise();
    }

    public getNumbersQuestions(): Promise<QuestionRecord[]> {
        return this._httpClient.get<QuestionRecord[]>(`${this._url}/questionCategories/2/questions`)
        .pipe(
            map(data => data['_embedded'].questions)
        )
        .toPromise();
    }

    public getPlacesQuestions(): Promise<QuestionRecord[]> {
        return this._httpClient.get<QuestionRecord[]>(`${this._url}/questionCategories/3/questions`)
        .pipe(
            map(data => data['_embedded'].questions)
        )
        .toPromise();
    }

    public getPrivateQuestions(): Promise<QuestionRecord[]> {
        return this._httpClient.get<QuestionRecord[]>(`${this._url}/questionCategories/4/questions`)
        .pipe(
            map(data => data['_embedded'].questions)
        )
        .toPromise();
    }
    get url(): string {
        return this._url;
    }
}
