/*
Copyright (c) 2021, Picpass solutions
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
* Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
* Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
* Neither the name of the Picpass solutions nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL Picpass solutions BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/
import { Injectable } from "@angular/core";
import { ConfigDataSource } from "./config.datasource";

@Injectable()
export class ConfigRepository {
    private _categories: string[] = [];
    private _defaultConfig: {};

    constructor(public _configSource: ConfigDataSource) {
      this.reconstruct();
    }

    public reconstruct() {
      this._categories = Object.keys(this._configSource.config);
      this._defaultConfig = ConfigRepository.deepClone<any>(this._configSource.config);
  }

    public resetKeywords(): string[] {
        this.initObj['Keywords:'] = this._defaultConfig['Initiation']['Keywords:'];
        this.initObj['Keywords:'] = this.initKeywords.slice(0, this.initMaxSelections);
        return this.initKeywords;
    }

    public resetPassword(): string {
        this.initPassword = this._defaultConfig['Initiation']['Password'];
        return this.initPassword;
    }

    get categories(): string[] {
        return this._categories;
    }

    get url(): string {
        let cmnctnObj = this.getConfigObject('Communication');
        cmnctnObj = cmnctnObj['Data source'];
        return `${cmnctnObj['Protocol']}://${cmnctnObj['Host']}:${cmnctnObj['Port']}${cmnctnObj['Root context']}`;
    }

    get initObj(): object {return this._defaultConfig['Initiation'];}

    set initObj(obj: object) {this._defaultConfig['Initiation'] = ConfigRepository.deepClone(obj);}


    get initRows(): number {return this.initObj['rows'][1]}

    get initColumns(): number {return this.initObj['cols'][1]}

    get initMaxSelections(): number {return this.initObj['maxSelections'][1]}

    get initLayoutSelections(): number {return this.initObj['layoutSelections'][1]}

    get initFrameMaxKeywords(): number {return this.initObj['frameKeywords'][1]}

    get initPassword(): string {return this.initObj['Password']}

    set initPassword(password: string) {this.initObj['Password'] = password}

    get initKeywords(): string[] {return this.initObj['Keywords:']}

    set initKeywords(kewordsStrArr: string[]) {this.initObj['Keywords:'] = kewordsStrArr}

    get initMaxWidth(): number {return this.initObj['maxWidth'][1]}

    get initPasswordInitalValue(): string {return this.initObj['PasswordInitalValue']}



    get featuresObj(): object {return this._defaultConfig['Features'];}

    set featuresObj(obj: object) {this._defaultConfig['Features'] = this._defaultConfig['Features'] = ConfigRepository.deepClone(obj);;}

    // implemented
    get featureCloseOnCompletion(): boolean {return this.featuresObj['quitOnCompletion'][1];}
    // implemented
    get featureProgressbarPostions(): number {return this.featuresObj['progrebarPostions'][1];}
    // implemented
    get featureImageSelections(): boolean  {return this.featuresObj['imageLayoutBegin'][1]}
    // implemented
    get featureExclusiveSubscriberSettings(): boolean {return this.featuresObj['exclusiveSubscriber'][1];}
    // implemented
    get featurePremiumSubscriberSettings(): boolean {return this.featuresObj['premiumSubscriber'][1];}
    // implemented
    get featureSubscriberSettings(): boolean {return this.featuresObj['subscriberSettings'][1];}
    // implemented
    get featureConfigurationSettings(): boolean {return this.featuresObj['configurationSettings'][1];}
    // implemented
    get featureColoredBorderIndicator(): boolean {return this.featuresObj['coloredBorder'][1];}
    // implemented
    get featureProgressbarPanel(): boolean {return this.featuresObj['progressbarPanel'][1];}
    // implemented
    get featureCompletionButton(): boolean {return this.featuresObj['quitButton'][1];}
    // implemented
    get featureRefreshLayoutButton(): boolean {return this.featuresObj['refreshButton'][1];}
    // implemented
    get featureLayoutToggleButton(): boolean {return this.featuresObj['toggleLayout'][1];}
    // implemented
    get featureImageHover(): boolean {return this.featuresObj['keywordsRevealHint'][1];}
    // implemented
    get featureHintPanel(): boolean {return this.featuresObj['hintPanel'][1];}
    // implemented
    get featureHintHover(): boolean {return this.featuresObj['imageHint'][1];}
    // implemented
    get featureHintText(): boolean {return this.featuresObj['textHint'][1];}
    // implemented
    get featureUpdatePassword(): boolean {return this.featuresObj['passwordConfiguration'][1];}
    // implemented
    get featurePasswordSubmit(): boolean {return this.featuresObj['submitPassword'][1];}
    // implemented
    get featureFeatureSettings(): boolean {return this.featuresObj['featureSettings'][1];}
    // implemented
    get featureInitiationSettings(): boolean {return this.featuresObj['initSettings'][1];}
    // implemented
    get featurePasswordValueReset(): boolean {return this.featuresObj['resetPassword'][1];}


    getConfigObject(categoryName: string): Object {
        return this._configSource.config[categoryName];
    }

    static deepClone<T>(a: T): T {
        return JSON.parse(JSON.stringify(a));
    }
}
